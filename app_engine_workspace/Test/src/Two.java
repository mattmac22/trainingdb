
public class Two extends One
{
	private String name;
	
	public Two(String name)
	{
		super(name);
		this.name = name;
	}
	public void getSuper()
	{
		System.out.println(super.name);
	}
	public static void main(String[] args)
	{
		Two t = new Two("John");
		t.name = "mark";
		t.getSuper();
		
	}
	

}
