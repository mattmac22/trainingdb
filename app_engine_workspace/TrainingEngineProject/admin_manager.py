'''
Created on Jun 28, 2014

@author: Matthew
'''
import models
from sqlalchemy.orm import sessionmaker
import validation
from service import BaseService
from backend_keys import Keys
import custom_exceptions
from sqlalchemy import func
from google.appengine.api import users

class AdminManager():
#======================= Credential CODE ===========================
    def add_credential(self, kv_pairs, session):
        validator = validation.CredentialValidator(session)
        validator.validate_kv_pairs(kv_pairs)
        credential = models.Credential(**kv_pairs)
        session.add(credential)
    def remove_credential_by_email(self, email, session):
        credential = session.query(models.Credential).filter(func.lower(models.Credential.email) == func.lower(email))[0]
        session.delete(credential)
    def get_permissions_by_email(self, email, session):
        credentials = session.query(models.Credential).filter(func.lower(models.Credential.email) == func.lower(email))
        if credentials.count() == 0:
            return "NONE"
        credential = credentials[0]
        result = ""
        if getattr(credential, Keys.API.perm_create):
            result += "C"
        if getattr(credential, Keys.API.perm_read):
            result += "R"
        if getattr(credential, Keys.API.perm_update):
            result += "U"
        if getattr(credential, Keys.API.perm_delete):
            result += "D"
        return result
    def credential_has_permissions(self, email, permissions, session):
        credentials = session.query(models.Credential).filter(func.lower(models.Credential.email) == func.lower(email))
        if credentials.count() == 0:
            return False
        credential = credentials[0]
        has_perm = True
        if Keys.Credential.perm_create in permissions:
            has_perm = has_perm and getattr(credential, Keys.Credential.perm_create)
        if Keys.Credential.perm_read in permissions:
            has_perm = has_perm and getattr(credential, Keys.Credential.perm_read)
        if Keys.Credential.perm_update in permissions:
            has_perm = has_perm and getattr(credential, Keys.Credential.perm_update)
        if Keys.Credential.perm_delete in permissions:
            has_perm = has_perm and getattr(credential, Keys.Credential.perm_delete)
        return has_perm
    def contains_credential(self, email, session):
        return session.query(models.Credential).filter(func.lower(models.Credential.email) == func.lower(email)).count() > 0
#======================= USER CODE ===========================
    def get_current_user_email(self):
        return users.get_current_user().email()
    def get_current_user_nickname(self):
        return (users.get_current_user().nickname())
    def get_current_user_permissions(self, session):
        return self.get_permissions_by_email(self.get_current_user_email(), session)
    def current_user_has_permissions(self, permissions, session):
        return self.credential_has_permissions(self.get_current_user_email(), permissions, session)
    def get_sign_out_url(self):
        return users.create_logout_url('/')
#======================= LOG CODE ===========================



