'''
Created on Jun 26, 2014

@author: Matthew
'''
def request_to_dict(request):
    dict = {}
    for arg in request.arguments():
        dict.update({arg: request.get(arg)})
    return dict
def request_to_kv_pairs(handler, FIELDS_ALL, FIELDS_FILE, FIELDS_CHECKBOX):
    kvPairs = {}
    for field in FIELDS_ALL:
        if field in FIELDS_CHECKBOX:
            value = handler.request.get(field) == 'True'
        elif field in FIELDS_FILE:
            files = handler.get_uploads(field)
            if len(files) == 0:
                continue;
            value = files[0].key()
        else:
            value = handler.request.get(field)
        kvPairs[field] = value
    return kvPairs

