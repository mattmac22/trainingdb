'''
Created on Aug 26, 2014

@author: Matthew
'''

from auth import permission_required
from auth import Permission
import webapp2
import data_manager
import jinja2
import os
from backend_keys import Keys
from service import BaseService
import handlers_util
import admin_manager
import service
import models
from StringIO import StringIO
import json
import gcs_utils

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__) + "/templates"),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

class URIProvider:
    def delete_user(self, user_id):
        return webapp2.uri_for('change_user_visibility',
                               user_id=user_id,
                               visible=False)
    def restore_user(self, user_id):
        return webapp2.uri_for('change_user_visibility',
                               user_id=user_id,
                               visible=True)
    def view_deleted_users(self):
        return webapp2.uri_for('view_users',
                               visible=False)
    def view_users(self):
        return webapp2.uri_for('view_users', visible=True)
uriProvider = URIProvider()

            
class ViewUsers(webapp2.RequestHandler):
    @permission_required([Permission.READ, Permission.CREATE]) 
    def get(self):
        visible_string = self.request.get("visible").strip() 
        visible = visible_string == "" or visible_string == "True"
        print str(visible)
        manager = data_manager.DataManager()
        manager_admin = admin_manager.AdminManager()
        template = JINJA_ENVIRONMENT.get_template('view_users.html')
        session = BaseService.Session()
        users = manager.get_users(session, visible=visible)
        self.response.out.write(template.render({
                                                 "sign_out_url": manager_admin.get_sign_out_url(),
                                                 "user_name": manager_admin.get_current_user_nickname(),
                                                 "users": users,
                                                 'User': Keys.User,
                                                 'COMMON': Keys.User.COMMON,
                                                 'FIELDS': Keys.User.FIELDS_DISPLAY,
                                                 'URIProvider': uriProvider,
                                                 'visible': visible,
                                                 'categories': manager.get_parent_categories(session, visible=(1 if visible else 0))}))
        
class ChangeUserVisibility(webapp2.RequestHandler):
    @permission_required([Permission.CREATE, Permission.UPDATE])
    def get(self):
        visible = self.request.get('visible') == 'True'
        user_id = int(self.request.get('user_id'))
        manager = data_manager.DataManager()
        session = service.BaseService.Session()
        try:
            manager.change_user_visibility(session, user_id, visible)
            session.commit()
        except Exception as e:
            session.rollback()
            self.response.out.write(str(e))
            return
        self.redirect(webapp2.uri_for('view_users'))
        