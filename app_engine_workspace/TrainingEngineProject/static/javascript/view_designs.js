/**
 * 
 */
function downloadBlob(blobKey)
{
	theUrl = "/serve?blob_key=" + blobKey;
	window.alert("URL: " + theUrl);
	var xmlHttp = null;
	xmlHttp = new XMLHttpRequest();
	xmlHttp.open("GET", theUrl, false);
	xmlHttp.send(null);
	return xmlHttp.responseText;
}
function showOligos(caller)
{
	var label_content = $(caller).closest('tr').find("#label_id").html();
	$.post("/api/getOligos",
			{api_key: "F94kD85j3h", label_id: label_content},
			function(data, textStatus, jqXHR)
			{
				window.myData = data;
				entryArray = $.parseJSON(data);
				$('#oligoTableBody').find('tr').remove();
				$.each(entryArray, function(index, entry){
					var tableRow = $(document.createElement('tr'));
					tableRow.data('entity_id', entry.key);
					var checkbox = $('<input>', {
						type:'checkbox',
						name: entry.name
					});
					checkbox.prop('checked', Boolean(entry.good_sequence));
					if (Boolean(entry.good_sequence) == false){
						console.log('setting');
						// Highlight row
						tableRow.addClass('selected');
					}
					checkbox.change(function(){
						if ($(this).is(':checked')){
							tableRow.css('background', '#FAFAFA')
						}
						else{
							tableRow.css('background','#FFDD51');
						}
					});
					checkbox.data('original', Boolean(entry.good_sequence));
					tableRow.append($(document.createElement('td')).append('Valid').append(checkbox));
					tableRow.append('<td>' + entry.sequence +'</td><td>' + entry.name +'</td>');
					var input = $('<input type="text">');
					input.val(entry.notes);
					input.data('original', entry.notes);
					tableRow.append($(document.createElement('td')).append(input));
					$('#oligoTableBody').append(tableRow);
				});
			    $('.fancybox').fancybox().trigger('click');

			}).fail(function(jqXHR, textStatus, errorThrown) 
			{
				alert("Unable to connect to server");
			});
}
function updateOligos()
{
	
	var update = new Array();
	$('#oligoTable > tbody').find('tr').each(function(){
		var checkbox = $(this).find('input:checkbox:first');
		var isChecked = checkbox.is(':checked');
		var needsValidUpdate = checkbox.data('original') != Boolean(isChecked);
		var textBox = $(this).find('input:text:first');
		var text = textBox.val();
		console.log(text);
		var needsTextUpdate = textBox.data('original') != text;
		var entityID = $(this).data('entity_id');
		if (needsTextUpdate || needsValidUpdate){
			var oligoUpdate = new Object();
			oligoUpdate.key = entityID;
			if (needsTextUpdate){
				oligoUpdate.notes = text;
			}
			if (needsValidUpdate){
				oligoUpdate.good_sequence = isChecked;
			}
			console.log(oligoUpdate);
			update.push(oligoUpdate);
		}
	});
	if (update.length != 0){
		json = JSON.stringify(update);
		$.post("/api/updateOligos",
				{api_key: "F94kD85j3h",'data':json},
				function(data, textStatus, jqXHR)
				{
					$.fancybox.close();
				}).fail(function(jqXHR, textStatus, errorThrown) 
				{
					alert("Unable to connect to server");
				});
	}
}
$(document).ready(function(){
	console.log('starting');
    $('.fancybox').fancybox();    
    
    $('#myTable').dataTable();
	var table = $('#myTable').DataTable();

	console.log('here');
	function format ( name, value ) {
	    return '<div><strong>' + name + ':</strong><br />' + value + '</div> </n>';
	}
    // Add event listener for opening and closing details
	$('td.details-control').unbind('click');
    $('td.details-control').click(function () {
    	console.log('here');
        var tr = $(this).closest('tr');
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(tr.data('notes-name'), tr.data('notes-value'))+
            		   format(tr.data('matlab-name'), tr.data('matlab-value'))+
            		   format(tr.data('design-notes-name'), tr.data('design-notes-value'))).show();
            tr.addClass('shown');
        }
     });
	
	
	
    $('#myTable tfoot th').each( function () {
        var title = $('#myTable thead th').eq( $(this).index() ).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );
 
    // Apply the filter
    table.columns().eq( 0 ).each( function ( colIdx ) {
        $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () {
            table
                .column( colIdx )
                .search( this.value )
                .draw();
        } );
    } );
	hideDiv();
	$('#collapseButton').off("click");
	$('#collapseButton').click({isCollapsed: true},divCallBack);
});

