/**
 * 
 */

$(document).ready(function() {

	/* Using custom settings */
	
	$(".fancybox").fancybox({

	});

	var table = $('#myTable').DataTable();

    $('#myTable tfoot th').each( function () {
        var title = $('#myTable thead th').eq( $(this).index() ).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );
 
    // Apply the filter
    table.columns().eq( 0 ).each( function ( colIdx ) {
        $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () {
            table
                .column( colIdx )
                .search( this.value )
                .draw();
        } );
    } );
});
function submitAssoc()
{
	$.fancybox.close();
	parent.$.fancybox.close();
	console.log("inside assoc");
	var content = ""
	$("#myTable tr").each(function(){
		var checkBox = $(this).find(".assocCheckBoxes")
		window.myCheck = checkBox;
		if (checkBox.is(':checked'))
		{
			console.log('checked');
			var value = $(this).find("#label_id").html();
			console.log("value: " + value);
			if (content == "")
			{
				content = value;
			}
			else
			{
				content = content + ", " + value;
			}	
		}
		else
			{
			console.log('not checked');
			}
	});
	$("#associated_design_ids").val(content);
}