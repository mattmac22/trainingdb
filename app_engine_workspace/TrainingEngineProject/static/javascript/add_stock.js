/**
 * 
 */
$(document).ready(function() {
    prepareSelectDesignDialog();
    console.log("in on windows ready");
});
//=======================================Select Design Dialog========================================
function prepareSelectDesignDialog()
{
	$(".fancybox").fancybox({});
	var table = $('#myTable').DataTable();
    $('#myTable tfoot th').each( function () {
        var title = $('#myTable thead th').eq( $(this).index() ).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );
 
    // Apply the filter
    table.columns().eq( 0 ).each( function ( colIdx ) {
        $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () {
            table
                .column( colIdx )
                .search( this.value )
                .draw();
        } );
    } );
    console.log("fancybox is prepared");
}

function selectDesign(caller)
{
	var content = $(caller).closest('tr').find("#label_id").html();
	var button = $(this);
	console.log(content);
	$("#probe_design_id").val(content);
	$.fancybox.close();
	$('#used_oligo_ids').val("");
	populateOligoTable();
}
//=======================================Oligos================================================
function updateOligos()
{
	var entities = new Array();
	var showAlert = false;
	$('.selectClass:checked').each(function(){
		var parentRow = $(this).closest('tr');
		var isGood = $(this).closest('tr').find('.isGoodClass').is(':checked');
		console.log(window.myCheck);
		console.log(isGood);
		if (isGood == false){
			showAlert = true;
		}
		var entityKey = parentRow.data('entity_id');
		entities.push(entityKey);
	});
	if (showAlert)
	{
		tf = confirm("Some of the selected oligos were marked as bad. Are you sure you want to continue?");
		if (tf){
			updateOligoField(entities);
		}
	}
	else
	{
		updateOligoField(entities);
	}
}
function updateOligoField(entities)
{
	if (entities.length > 0){
		var encoded = JSON.stringify(entities);
		console.log(encoded);
		$('#used_oligo_ids').val(encoded);
		$.fancybox.close();
	}
	else{
		$('#used_oligo_ids').val("");
		$.fancybox.close();
	}	
}

function populateOligoTable(caller)
{
	console.log('populating oligo table');
	var label_content = $("#probe_design_id").val();
	window.myLabel = label_content;
	$.post("/api/getOligos",
			{api_key: "F94kD85j3h", label_id: label_content},
			function(data, textStatus, jqXHR)
			{
				console.log(data);
				entryArray = $.parseJSON(data);
				$('#oligoTableBody').find('tr').remove();
				$.each(entryArray, function(index, entry){
					var tableRow = $(document.createElement('tr'));
					tableRow.data('entity_id', entry.key);
					var selectBox = $('<input>', {
						type:'checkbox',
						name: 'Select',
						checked: false
					});
					selectBox.addClass('selectClass');
					tableRow.append($(document.createElement('td')).append(selectBox));
					var checkbox = $('<input>', {
						type:'checkbox',
						name: entry.name,
						onclick: "return false"
					});
					checkbox.addClass('isGoodClass')
					checkbox.prop('checked', Boolean(entry.good_sequence));
					if (Boolean(entry.good_sequence) == false){
						console.log('setting');
						tableRow.css('background', '#FFDD51 !important');
					}
					checkbox.change(function(){
						if ($(this).is(':checked')){
							tableRow.css('background', '#FAFAFA')
						}
						else{
							tableRow.css('background','#FFDD51');
						}
					});
					checkbox.data('original', Boolean(entry.good_sequence));
					tableRow.append($(document.createElement('td')).append('Valid').append(checkbox));
					tableRow.append('<td>' + entry.sequence +'</td><td>' + entry.name +'</td>');
					var input = $('<input type="text" readonly>');
					input.val(entry.notes);
					input.data('original', entry.notes);
					tableRow.append($(document.createElement('td')).append(input));
					$('#oligoTableBody').append(tableRow);
				});
			    //$('#selectOligosLinkID').fancybox().trigger('click');

			}).fail(function(jqXHR, textStatus, errorThrown) 
			{
				alert("Unable to connect to server");
			});
}