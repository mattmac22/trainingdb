/**
 * 
 */
function downloadBlob(blobKey)
{
	theUrl = "/serve?blob_key=" + blobKey;
	window.alert("URL: " + theUrl);
	var xmlHttp = null;
	xmlHttp = new XMLHttpRequest();
	xmlHttp.open("GET", theUrl, false);
	xmlHttp.send(null);
	return xmlHttp.responseText;
}
function showOligos(caller)
{
	var label_id = $(caller).closest('tr').find("#label_id").html();
	var probe_design_id = $(caller).closest('tr').find("#probe_design_id").html();
	console.log(label_id);
	console.log(probe_design_id);
	$.post("/api/getOligosForStock",
			{api_key: "F94kD85j3h", stock_label_id: label_id, probe_label_id: probe_design_id},
			function(data, textStatus, jqXHR)
			{
				window.myData = data;
				entryArray = $.parseJSON(data);
				$('#oligoTableBody').find('tr').remove();
				$.each(entryArray, function(index, entry){
					var tableRow = $(document.createElement('tr'));
					tableRow.data('entity_id', entry.key);
					var checkbox = $('<input>', {
						type:'checkbox',
						name: entry.name
					});
					checkbox.prop('checked', Boolean(entry.good_sequence));
					if (Boolean(entry.good_sequence) == false){
						console.log('setting');
						// Highlight row
						tableRow.addClass('selected');
					}
					checkbox.change(function(){
						if ($(this).is(':checked')){
							tableRow.css('background', '#FAFAFA')
						}
						else{
							tableRow.css('background','#FFDD51');
						}
					});
					checkbox.data('original', Boolean(entry.good_sequence));
					tableRow.append($(document.createElement('td')).append('Valid').append(checkbox));
					tableRow.append('<td>' + entry.sequence +'</td><td>' + entry.name +'</td>');
					var input = $('<input type="text">');
					input.val(entry.notes);
					input.data('original', entry.notes);
					tableRow.append($(document.createElement('td')).append(input));
					$('#oligoTableBody').append(tableRow);
				});
			    $('.fancybox').fancybox().trigger('click');

			}).fail(function(jqXHR, textStatus, errorThrown) 
			{
				alert("Unable to connect to server");
			});
}
function updateOligos()
{
	
	var update = new Array();
	$('#oligoTable > tbody').find('tr').each(function(){
		var checkbox = $(this).find('input:checkbox:first');
		var isChecked = checkbox.is(':checked');
		var needsValidUpdate = checkbox.data('original') != Boolean(isChecked);
		var textBox = $(this).find('input:text:first');
		var text = textBox.val();
		console.log(text);
		var needsTextUpdate = textBox.data('original') != text;
		var entityID = $(this).data('entity_id');
		if (needsTextUpdate || needsValidUpdate){
			var oligoUpdate = new Object();
			oligoUpdate.key = entityID;
			if (needsTextUpdate){
				oligoUpdate.notes = text;
			}
			if (needsValidUpdate){
				oligoUpdate.good_sequence = isChecked;
			}
			console.log(oligoUpdate);
			update.push(oligoUpdate);
		}
	});
	if (update.length != 0){
		json = JSON.stringify(update);
		$.post("/api/updateOligos",
				{api_key: "F94kD85j3h",'data':json},
				function(data, textStatus, jqXHR)
				{
					$.fancybox.close();
				}).fail(function(jqXHR, textStatus, errorThrown) 
				{
					alert("Unable to connect to server");
				});
	}
}
$(document).ready(function(){

    $('.fancybox').fancybox();    
    
    $('#myTable').dataTable();
	var table = $('#myTable').DataTable();

	
    $('#myTable tfoot th').each( function () {
        var title = $('#myTable thead th').eq( $(this).index() ).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );
 
    // Apply the filter
    table.columns().eq( 0 ).each( function ( colIdx ) {
        $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () {
            table
                .column( colIdx )
                .search( this.value )
                .draw();
        } );
    } );
	hideDiv();
	$('#collapseButton').off("click");
	$('#collapseButton').click({isCollapsed: true},divCallBack);
});

