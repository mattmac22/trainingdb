'''
Created on Aug 26, 2014

@author: Matthew
'''

from auth import permission_required
from auth import Permission
import webapp2
import data_manager
import jinja2
import os
from backend_keys import Keys
from service import BaseService
import handlers_util
import admin_manager
import service
import models
from StringIO import StringIO
import json
import gcs_utils

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__) + "/templates"),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

class URIProvider:
    def delete_dog(self, dog_id):
        return webapp2.uri_for('change_dog_visibility',
                               dog_id=dog_id,
                               visible=False)
    def restore_dog(self, dog_id):
        return webapp2.uri_for('change_dog_visibility',
                               dog_id=dog_id,
                               visible=True)
    def get_image_thumb_for_dog_id(self, dog_id):
        return webapp2.uri_for('get_thumb_image_for_dog_id',
                               dog_id=dog_id)
    def view_deleted_dogs(self):
        return webapp2.uri_for('view_dogs', visible=False)
    def view_dogs(self):
        return webapp2.uri_for('view_dogs', visible=True)
    def view_entries_for_dog(self, dog_id):
        return webapp2.uri_for('view_entries', _visible=True, dog_id=dog_id)
uriProvider = URIProvider()

class GetThumbImageForDogID(webapp2.RequestHandler):
    def get(self):
        dog_id = self.request.get("dog_id")
        image_name = "dog_image_thumb_" + str(dog_id)
        image = gcs_utils.read_file(image_name)
        self.response.headers['Content-Type'] = 'image/jpeg'
        self.response.out.write(image)

            
class ViewDogs(webapp2.RequestHandler):
    @permission_required([Permission.READ, Permission.CREATE]) 
    def get(self):
        visible_string = self.request.get("visible").strip() 
        visible = visible_string == "" or visible_string == "True"
        print str(visible)
        manager = data_manager.DataManager()
        manager_admin = admin_manager.AdminManager()
        template = JINJA_ENVIRONMENT.get_template('view_dogs.html')
        session = BaseService.Session()
        dogs = manager.get_dogs(session, include_image=False, visible=visible)
        self.response.out.write(template.render({
                                                 "sign_out_url": manager_admin.get_sign_out_url(),
                                                 "user_name": manager_admin.get_current_user_nickname(),
                                                 "dogs": dogs,
                                                 'Dog': Keys.Dog,
                                                 'COMMON': Keys.Dog.COMMON,
                                                 'FIELDS': Keys.Dog.FIELDS_DISPLAY,
                                                 'URIProvider': uriProvider,
                                                 'visible': visible, 
                                                 }))
        
class ChangeDogVisibility(webapp2.RequestHandler):
    @permission_required([Permission.CREATE, Permission.UPDATE])
    def get(self):
        visible = self.request.get('visible') == 'True'
        dog_id = int(self.request.get('dog_id'))
        manager = data_manager.DataManager()
        session = service.BaseService.Session()
        try:
            manager.change_dog_visibility(session, dog_id, visible)
            session.commit()
        except Exception as e:
            session.rollback()
            self.response.out.write(str(e))
            return
        self.redirect(webapp2.uri_for('view_dogs'))
        