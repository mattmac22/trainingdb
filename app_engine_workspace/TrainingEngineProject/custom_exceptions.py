'''
Created on Jun 21, 2014

@author: Matthew
'''
class TransactionException(Exception):
    def __init__(self, message):
        Exception.__init__(self, message)
        
class InvalidField(TransactionException):
    def __init__(self, message):
        Exception.__init__(self, message)
        
class MissingField(TransactionException):
    def __init__(self, message):
        Exception.__init__(self, message)

class DogDoesNotExist(TransactionException):
    def __init__(self, message):
        Exception.__init__(self, message)
class UserDoesNotExist(TransactionException):
    def __init__(self, message):
        Exception.__init__(self, message)
class ListNotUnique(TransactionException):
    def __init__(self, list_name):
        Exception.__init__(self, "List not unique " + list_name)
class DuplicateEntry(TransactionException):
    def __init__(self, list_name):
        Exception.__init__(self, "Duplicate entry " + list_name)
class ConsistencyException(TransactionException):
    def __init__(self, message):
        Exception.__init__(self, message)
    