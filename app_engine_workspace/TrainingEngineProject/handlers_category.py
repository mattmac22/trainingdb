'''
Created on Jun 27, 2014

@author: Matthew
'''
from auth import permission_required
from auth import Permission
import webapp2
import data_manager
import jinja2
import os
from backend_keys import Keys
from service import BaseService
import handlers_util
import admin_manager
import service
import models
from StringIO import StringIO
import json

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__) + "/templates"),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

class URIProvider:
    def delete_sub_category(self, sub_category_id):
        return webapp2.uri_for('change_sub_category_visibility',
                               sub_category_id=sub_category_id,
                               visible=False)
    def restore_sub_category(self, sub_category_id):
        return webapp2.uri_for('change_sub_category_visibility',
                               sub_category_id=sub_category_id,
                               visible=True)
    def view_sub_categories(self, parent_category_id):
        return webapp2.uri_for('view_sub_categories',
                               parent_category_id=parent_category_id)
    def delete_parent_category(self, parent_category_id):
        return webapp2.uri_for('change_parent_category_visibility', 
                               parent_category_id=parent_category_id,
                               visible=False)
    def restore_parent_category(self, parent_category_id):
        return webapp2.uri_for('change_parent_category_visibility', 
                               parent_category_id=parent_category_id,
                               visible=True)
    
uriProvider = URIProvider()
#============================== PARENT CATEGORY ====================================        
class AddParentCategoryHandler(webapp2.RequestHandler):
    @permission_required([Permission.READ, Permission.CREATE]) 
    def get(self):
        manager = data_manager.DataManager()
        manager_admin = admin_manager.AdminManager()
        template = JINJA_ENVIRONMENT.get_template('add_parent_category.html')
        self.response.out.write(template.render({
                                                 "sign_out_url": manager_admin.get_sign_out_url(),
                                                 "user_name": manager_admin.get_current_user_nickname(),
                                                 'ParentCategory': Keys.ParentCategory,
                                                 'upload_url': '/user/addParentCategoryHandler'
                                                 })) 
    @permission_required([Permission.READ, Permission.CREATE]) 
    def post(self):
        kv_pairs = handlers_util.request_to_dict(self.request)
        manager = data_manager.DataManager()
        session = BaseService.Session()
        try:
            manager.add_parent_category(kv_pairs, session)
            session.commit()
        except Exception as e:
            session.rollback()
            self.response.out.write(str(e))
            return
        self.redirect('/user/viewParentCategories') 
class ViewParentCategoriesHandler(webapp2.RequestHandler):
    @permission_required([Permission.READ, Permission.CREATE]) 
    def get(self):
        visible_string = self.request.get("visible").strip() 
        visible = visible_string == "" or visible_string == "True"
        print str(visible)
        manager = data_manager.DataManager()
        manager_admin = admin_manager.AdminManager()
        template = JINJA_ENVIRONMENT.get_template('view_parent_categories.html')
        session = BaseService.Session()
        self.response.out.write(template.render({
                                                 "sign_out_url": manager_admin.get_sign_out_url(),
                                                 "user_name": manager_admin.get_current_user_nickname(),
                                                 'ParentCategory': Keys.ParentCategory,
                                                 'FIELDS_COMMON': Keys.ParentCategory.DISPLAY_FIELDS_COMMON,
                                                 'FIELDS': Keys.ParentCategory.DISPLAY_FIELDS,
                                                 'URIProvider': uriProvider,
                                                 'visible': visible,
                                                 'categories': manager.get_parent_categories(session, visible=(1 if visible else 0))}))
class DeleteParentCategory(webapp2.RequestHandler):
    @permission_required([Permission.CREATE, Permission.UPDATE])
    def post(self):
        parent_id = int(self.request.get("parent_category_id"))
        manager = data_manager.DataManager()
        session = service.BaseService.Session()
        try:
            manager.delete_parent_category_by_id(session, parent_id)
            session.commit()
        except Exception as e:
            session.rollback()
            self.response.out.write(str(e))
            return
        self.response.out.write("success")
class ChangeParentCategoryVisibility(webapp2.RequestHandler):
    @permission_required([Permission.CREATE, Permission.UPDATE])
    def get(self):
        visible = self.request.get('visible') == 'True'
        parent_category_id = int(self.request.get('parent_category_id'))
        manager = data_manager.DataManager()
        session = service.BaseService.Session()
        try:
            manager.change_parent_category_visibility(session, parent_category_id, visible)
            session.commit()
        except Exception as e:
            session.rollback()
            self.response.out.write(str(e))
            return
        self.redirect(webapp2.uri_for('view_parent_categories'))
        
#============================== SUB CATEGORY ====================================  

class AddSubCategoryHandler(webapp2.RequestHandler):
    @permission_required([Permission.READ, Permission.CREATE]) 
    def get(self):
        manager = data_manager.DataManager()
        manager_admin = admin_manager.AdminManager()
        parent_category_id = int(self.request.get("parent_category_id"))
        template = JINJA_ENVIRONMENT.get_template('add_sub_category.html')
        self.response.out.write(template.render({
                                                 "sign_out_url": manager_admin.get_sign_out_url(),
                                                 "user_name": manager_admin.get_current_user_nickname(),
                                                 'SubCategory': Keys.SubCategory,
                                                 "parent_category_id": parent_category_id,
                                                 'upload_url': '/user/addParentCategoryHandler'
                                                 })) 
    @permission_required([Permission.READ, Permission.CREATE]) 
    def post(self):
        kv_pairs = handlers_util.request_to_dict(self.request)
        manager = data_manager.DataManager()
        session = BaseService.Session()
        print kv_pairs
        try:
            manager.add_sub_category(kv_pairs, session)
            session.commit()
        except Exception as e:
            session.rollback()
            self.response.out.write(str(e))
            return
        self.response.out.write('success')
class UpdateSubCategoryHandler(webapp2.RequestHandler):
    @permission_required([Permission.READ, Permission.CREATE]) 
    def get(self):
        manager = data_manager.DataManager()
        manager_admin = admin_manager.AdminManager()
        sub_category_id = int(self.request.get("sub_category_id"))
        session = BaseService.Session()
        sub_category = manager.get_sub_categories(session, {Keys.SubCategory.id: sub_category_id})[0]
        template = JINJA_ENVIRONMENT.get_template('update_sub_category.html')
        plan = json.load(StringIO(sub_category[Keys.SubCategory.plan]))
        self.response.out.write(template.render({
                                                 "sign_out_url": manager_admin.get_sign_out_url(),
                                                 "user_name": manager_admin.get_current_user_nickname(),
                                                 'SubCategory': Keys.SubCategory,
                                                 'parent_category_id': sub_category[Keys.SubCategory.parent_category_id],
                                                 'sub_category_id': sub_category[Keys.SubCategory.id],
                                                 'sub_category_name': sub_category[Keys.SubCategory.name],
                                                 'plan': plan,
                                                 })) 
    @permission_required([Permission.READ, Permission.CREATE]) 
    def post(self):
        kv_pairs = handlers_util.request_to_dict(self.request)
        # Hide old one
        former_sub_category_id = kv_pairs[Keys.SubCategory.id]
        del kv_pairs[Keys.SubCategory.id]
        manager = data_manager.DataManager()
        session = BaseService.Session()
        try:
            manager.change_sub_category_visibility(session, former_sub_category_id, visible=False)
            session.commit()
        except Exception as e:
            session.rollback()
            self.response.out.write(str(e))
            return
        # Create new one
        try:
            manager.add_sub_category(kv_pairs, session)
            session.commit()
        except Exception as e:
            session.rollback()
            self.response.out.write(str(e))
            return
        self.response.out.write('success')
class ViewSubCategoriesHandler(webapp2.RequestHandler):
    @permission_required([Permission.READ, Permission.CREATE]) 
    def get(self):
        visible_string = self.request.get("visible").strip() 
        visible = visible_string == "" or visible_string == "True"
        parent_category_id = int(self.request.get("parent_category_id"))
        manager = data_manager.DataManager()
        manager_admin = admin_manager.AdminManager()
        template = JINJA_ENVIRONMENT.get_template('view_sub_categories.html')
        session = BaseService.Session()
        categories = manager.get_sub_categories_by_parent_id(session, parent_category_id, {Keys.SubCategory.visible: (1 if visible else 0)})
        self.response.out.write(template.render({
                                                 "sign_out_url": manager_admin.get_sign_out_url(),
                                                 "user_name": manager_admin.get_current_user_nickname(),
                                                 'ParentCategory': Keys.ParentCategory,
                                                 "parent_category_id": parent_category_id,
                                                 'COMMON': models.SubCategory.DISPLAY_COMMON,
                                                 'URIProvider': uriProvider,
                                                 'FIELDS': models.SubCategory.DISPLAY_FIELDS,
                                                 'visible': visible,
                                                 'categories': categories}))
class ChangeSubCategoryVisibility(webapp2.RequestHandler):
    @permission_required([Permission.READ, Permission.CREATE, Permission.DELETE])
    def get(self):
        print "Changing Visibility"
        visible = self.request.get('visible') == 'True'
        print visible
        sub_category_id = int(self.request.get('sub_category_id'))
        session = BaseService.Session()
        manager = data_manager.DataManager()
        sub_category = manager.get_sub_categories(session, {Keys.SubCategory.id: sub_category_id})[0]
        parent_category_id = sub_category[Keys.SubCategory.parent_category_id]
        try:
            print "Changin sub_category with id " + str(sub_category_id) + " to visible of " + str(visible)
            manager.change_sub_category_visibility(session, sub_category_id, visible)
            session.commit()
        except Exception as e:
            session.rollback()
            self.response.out.write(str(e))
            return
        self.redirect(uriProvider.view_sub_categories(parent_category_id))
class ViewSubCategoryPlan(webapp2.RequestHandler):
    @permission_required([Permission.READ, Permission.CREATE]) 
    def get(self):
        manager = data_manager.DataManager()
        manager_admin = admin_manager.AdminManager()
        sub_category_id = int(self.request.get("sub_category_id"))
        session = BaseService.Session()
        sub_category = manager.get_sub_categories(session, {Keys.SubCategory.id: sub_category_id})[0]
        template = JINJA_ENVIRONMENT.get_template('view_sub_category_plan.html')
        plan = json.load(StringIO(sub_category[Keys.SubCategory.plan]))
        self.response.out.write(template.render({
                                                 "sign_out_url": manager_admin.get_sign_out_url(),
                                                 "user_name": manager_admin.get_current_user_nickname(),
                                                 'SubCategory': Keys.SubCategory,
                                                 'parent_category_id': sub_category[Keys.SubCategory.parent_category_id],
                                                 'sub_category_id': sub_category[Keys.SubCategory.id],
                                                 'sub_category_name': sub_category[Keys.SubCategory.name],
                                                 'plan': plan,
                                                 })) 

        
