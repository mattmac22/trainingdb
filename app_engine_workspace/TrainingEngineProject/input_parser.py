'''
Created on Jun 26, 2014

@author: Matthew
'''
import json
from StringIO import StringIO
import copy
import custom_exceptions
''' 
STRUCTURE OF PLAN
- array of Question objects
- each Question has a "name" string and "answers" array
- answers is an array of strings
'''
class PlanParser():
    def validate_plan(self, questions):
        names = []
        for question in questions:
            names.append(question["name"])
            if not self.list_unique(question["answers"]):
                raise custom_exceptions.ListNotUnique(question["name"])
        if not self.list_unique(names):
            raise custom_exceptions.ListNotUnique("names")
    def list_unique(self, my_list):
        return len(my_list) == len(set(my_list))
    def __tokenize_answers_array(self, answers):
        answers_tokenized = []
        for answer in answers:
            answer.strip()
            answer_token = answer.lower().replace(" ","_")
            answers_tokenized.append(answer)
            answers_tokenized.append(answer_token)
        return answers_tokenized
            
        