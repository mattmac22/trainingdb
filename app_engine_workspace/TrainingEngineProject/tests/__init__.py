'''
Created on Jun 24, 2014

@author: Matthew
'''
from sqlalchemy.engine import create_engine
from sqlalchemy.orm.session import Session

from models import Base  # This is your declarative base class


def setup_module():
    global transaction, connection, engine
    print "setting up module"
    # Connect to the database and create the schema within a transaction
    engine = create_engine('mysql://root:mag1c@localhost/trainingdb?charset=utf8&use_unicode=0', pool_recycle=3600)
    connection = engine.connect()
    transaction = connection.begin()
    Base.metadata.create_all(connection)

    # If you want to insert fixtures to the DB, do it here


def teardown_module():
    # Roll back the top level transaction and disconnect from the database
    transaction.rollback()
    connection.close()
    engine.dispose()
    
class DatabaseTest(object):
    def setup(self):
        self.__transaction = connection.begin_nested()
        self.session = Session(connection)

    def teardown(self):
        self.session.close()
        self.__transaction.rollback()