'''
Created on Jun 24, 2014

@author: Matthew
'''
'''
Make sure you have an empty database for unit testing
Create the engine, create one connection and start a transaction in it
Create the tables
Optionally, insert test fixtures
For every test, repeat:
    Create a savepoint
    Run the test
    Roll back to the savepoint
Roll back the transaction
'''
import unittest
from service import BaseService
import models
from backend_keys import Keys
import data_manager
import datetime

class Test(unittest.TestCase):
    
    def setUp(self):
        global transaction, connection, engine
        # Connect to the database and create the schema within a transaction
        models.create_tables(BaseService.engine_unit_test)
        self.session = BaseService.Session_unit_test()
        self.session.begin_nested()
        
    def tearDown(self):
        self.session.rollback()
        models.drop_all_tables(BaseService.engine_unit_test)
        self.session.close()
    
    def testAddTwoSubCategoryToSameParent(self):
        manager = data_manager.DataManager()
        manager.add_parent_category({Keys.ParentCategory.name: "cat1"}, self.session)
        manager.add_sub_category({Keys.SubCategory.parent_category_id: 1,
                                  Keys.SubCategory.name: "subcat1",
                                  Keys.SubCategory.plan: "plan1"}, self.session)
        manager.add_sub_category({Keys.SubCategory.parent_category_id: 1,
                                  Keys.SubCategory.name: "subcat2",
                                  Keys.SubCategory.plan: "plan2"}, self.session)
        subcats = manager.get_sub_categories(self.session)
        print manager.get_category_compact(self.session)
        

        

        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()