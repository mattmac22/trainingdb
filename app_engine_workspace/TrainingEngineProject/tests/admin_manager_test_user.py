'''
Created on Jun 24, 2014

@author: Matthew
'''
'''
Make sure you have an empty database for unit testing
Create the engine, create one connection and start a transaction in it
Create the tables
Optionally, insert test fixtures
For every test, repeat:
    Create a savepoint
    Run the test
    Roll back to the savepoint
Roll back the transaction
'''
import unittest
from service import BaseService
import models
from backend_keys import Keys
import data_manager
import datetime
import custom_exceptions
import admin_manager
from google.appengine.ext import testbed

class Test(unittest.TestCase):
    
    def setUp(self):
        global transaction, connection, engine
        # Connect to the database and create the schema within a transaction
        models.create_tables(BaseService.engine_unit_test)
        self.session = BaseService.Session_unit_test()
        self.session.begin_nested()
        
        self.testbed = testbed.Testbed()
        # USER AUTHENTICATION STUFF
        self.testbed.setup_env(USER_EMAIL="admin@gmail.com",USER_ID='1')
        # Then activate the testbed, which prepares the service stubs for use.
        self.testbed.activate()
        
    def tearDown(self):
        self.session.rollback()
        models.drop_all_tables(BaseService.engine_unit_test)
        self.session.close()
    def testAddOne(self):
        manager = admin_manager.AdminManager()
        manager.add_credential({Keys.Credential.email: "admin@gmail.com"}, self.session)
        perms = manager.get_current_user_permissions(self.session)
        self.assertEqual(perms, "CRUD")
    def testNotAllPerms(self):
        manager = admin_manager.AdminManager()
        manager.add_credential({Keys.Credential.email: "admin@gmail.com",
                                Keys.Credential.perm_create: False,
                                Keys.Credential.perm_read: True,
                                Keys.Credential.perm_update: False,
                                Keys.Credential.perm_delete: True}, self.session)
        perms = manager.get_current_user_permissions(self.session)
        self.assertEqual(perms, "RD")
    def testHasPermissions(self):
        manager = admin_manager.AdminManager()
        manager.add_credential({Keys.Credential.email: "admin@gmail.com",
                                Keys.Credential.perm_create: False,
                                Keys.Credential.perm_read: True,
                                Keys.Credential.perm_update: False,
                                Keys.Credential.perm_delete: True}, self.session)
        self.assertFalse(manager.current_user_has_permissions("admin@gmail.com", [Keys.Credential.perm_create], self.session))
        self.assertTrue(manager.current_user_has_permissions("admin@gmail.com", [Keys.Credential.perm_read, Keys.Credential.perm_delete], self.session))
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
    
    
    
    