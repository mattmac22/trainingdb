'''
Created on Jun 24, 2014

@author: Matthew
'''
'''
Make sure you have an empty database for unit testing
Create the engine, create one connection and start a transaction in it
Create the tables
Optionally, insert test fixtures
For every test, repeat:
    Create a savepoint
    Run the test
    Roll back to the savepoint
Roll back the transaction
'''
import unittest
from service import BaseService
import models
from backend_keys import Keys
import data_manager
import datetime

class Test(unittest.TestCase):
    
    def setUp(self):
        global transaction, connection, engine
        # Connect to the database and create the schema within a transaction
        models.create_tables(BaseService.engine_unit_test)
        self.session = BaseService.Session_unit_test()
        self.session.begin_nested()
        
    def tearDown(self):
        self.session.rollback()
        models.drop_all_tables(BaseService.engine_unit_test)
        self.session.close()
    
    def testAddOneUser(self):
        manager = data_manager.DataManager()
        kv_pairs = self.get_dummy_users(1)[0]
        manager.add_user(kv_pairs, self.session)
        assert(manager.get_num_users(self.session) == 1)
        assert(kv_pairs[Keys.User.email] == "email1")
        assert(kv_pairs[Keys.User.phone] == "phone1")
        self.session.commit()
        
    def testAddNoUsers(self):
        manager = data_manager.DataManager()
        assert(manager.get_num_users(self.session) == 0)
        self.session.commit()
    def testAddThreeUsers(self):
        manager = data_manager.DataManager()
        users = self.get_dummy_users(3)
        manager.add_user(users[0], self.session)
        manager.add_user(users[1], self.session)
        manager.add_user(users[2], self.session)
        assert(users[0][Keys.User.email] == "email1")
        assert(users[1][Keys.User.email] == "email2")
        assert(users[2][Keys.User.email] == "email3")
        self.session.commit()
    def testGetUserNameByID(self):
        manager = data_manager.DataManager()
        users = self.get_dummy_users(3)
        manager.add_user(users[0], self.session)
        manager.add_user(users[1], self.session)
        manager.add_user(users[2], self.session)
        name2 = manager.get_user_name_from_id(self.session, 2)
        self.assertEqual("fullname2", name2)
    def testChangeUserVisibility(self):
        manager = data_manager.DataManager()
        users = self.get_dummy_users(3)
        manager.add_user(users[0], self.session)
        manager.add_user(users[1], self.session)
        manager.add_user(users[2], self.session)
        manager.change_user_visibility(self.session, 2, False)
        users = manager.get_users(self.session)
        self.assertEqual(2, len(users))
        users = manager.get_users(self.session, visible=False)
        self.assertEqual(1, len(users))
    def testTime(self):
        manager = data_manager.DataManager()
        users = self.get_dummy_users(3)
        manager.add_user(users[0], self.session)
        users = self.session.query(models.User).all()
        print datetime.datetime.now()
        for user in users:
            print user.last_updated

        
    def get_dummy_users(self, num_users):
        kv_pairs1 = {Keys.User.email: "email1", 
                     Keys.User.user_name: "username1",
                     Keys.User.full_name: "fullname1",
                     Keys.User.password: "password1",
                     Keys.User.phone: "phone1"}
        if num_users == 1:
            return [kv_pairs1]
        kv_pairs2 = {Keys.User.email: "email2", 
                     Keys.User.user_name: "username2",
                     Keys.User.full_name: "fullname2",
                     Keys.User.password: "password2",
                     Keys.User.phone: "phone2"}
        if num_users == 2:
            return [kv_pairs1, kv_pairs2]
        kv_pairs3 = {Keys.User.email: "email3", 
                     Keys.User.user_name: "username3",
                     Keys.User.full_name: "fullname3",
                     Keys.User.password: "password3",
                     Keys.User.phone: "phone3"}
        if num_users == 3:
            return [kv_pairs1, kv_pairs2, kv_pairs3]
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()