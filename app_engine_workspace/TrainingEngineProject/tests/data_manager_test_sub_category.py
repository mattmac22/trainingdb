'''
Created on Jun 24, 2014

@author: Matthew
'''
'''
Make sure you have an empty database for unit testing
Create the engine, create one connection and start a transaction in it
Create the tables
Optionally, insert test fixtures
For every test, repeat:
    Create a savepoint
    Run the test
    Roll back to the savepoint
Roll back the transaction
'''
import unittest
from service import BaseService
import models
from backend_keys import Keys
import data_manager
import datetime
import time

class Test(unittest.TestCase):
    
    def setUp(self):
        global transaction, connection, engine
        # Connect to the database and create the schema within a transaction
        models.create_tables(BaseService.engine_unit_test)
        self.session = BaseService.Session_unit_test()
        self.session.begin_nested()
        
    def tearDown(self):
        self.session.rollback()
        models.drop_all_tables(BaseService.engine_unit_test)
        self.session.close()
    
    def testAddTwoSubCategoryToSameParent(self):
        manager = data_manager.DataManager()
        manager.add_parent_category({Keys.ParentCategory.name: "cat1"}, self.session)
        manager.add_sub_category({Keys.SubCategory.parent_category_id: 1,
                                  Keys.SubCategory.name: "subcat1",
                                  Keys.SubCategory.plan: "plan1"}, self.session)
        manager.add_sub_category({Keys.SubCategory.parent_category_id: 1,
                                  Keys.SubCategory.name: "subcat2",
                                  Keys.SubCategory.plan: "plan2"}, self.session)
        subcats = manager.get_sub_categories(self.session)
        self.assertEqual(len(subcats), 2, 'length')
        self.assertEqual(subcats[0][Keys.SubCategory.parent_category_id], 1)
        self.assertEqual(subcats[0][Keys.SubCategory.name], "subcat1")
        self.assertEqual(subcats[1][Keys.SubCategory.name], "subcat2")
        
    def testAddToDifferentParents(self):
        manager = data_manager.DataManager()
        manager.add_parent_category({Keys.ParentCategory.name: "cat1"}, self.session)
        manager.add_parent_category({Keys.ParentCategory.name: "cat2"}, self.session)
        manager.add_sub_category({Keys.SubCategory.parent_category_id: 1,
                                  Keys.SubCategory.name: "subcat1",
                                  Keys.SubCategory.plan: "plan1"}, self.session)
        manager.add_sub_category({Keys.SubCategory.parent_category_id: 2,
                                  Keys.SubCategory.name: "subcat2",
                                  Keys.SubCategory.plan: "plan2"}, self.session)
        subcat1 = manager.get_sub_categories_by_parent_id(self.session, 1)[0]
        subcat2 = manager.get_sub_categories_by_parent_id(self.session, 2)[0]
        self.assertEqual(subcat1[Keys.SubCategory.name], "subcat1")
        self.assertEqual(subcat2[Keys.SubCategory.name], "subcat2")

    def testChangeVisibility(self):
        manager = data_manager.DataManager()
        manager.add_parent_category({Keys.ParentCategory.name: "cat1"}, self.session)
        manager.add_parent_category({Keys.ParentCategory.name: "cat2"}, self.session)
        manager.add_sub_category({Keys.SubCategory.parent_category_id: 1,
                                  Keys.SubCategory.name: "subcat1",
                                  Keys.SubCategory.plan: "plan1"}, self.session)
        manager.add_sub_category({Keys.SubCategory.parent_category_id: 2,
                                  Keys.SubCategory.name: "subcat2",
                                  Keys.SubCategory.plan: "plan2"}, self.session)
        
        manager.change_sub_category_visibility(self.session, 2, False)
        
        subcat2 = manager.get_sub_categories(self.session, {Keys.SubCategory.id: 2})[0]
        self.assertEqual(subcat2[Keys.SubCategory.name], 'subcat2', 'subcat2ab')
    def testGetSubCategoryName(self):
        manager = data_manager.DataManager()
        manager.add_parent_category({Keys.ParentCategory.name: "cat1"}, self.session)
        manager.add_parent_category({Keys.ParentCategory.name: "cat2"}, self.session)
        manager.add_sub_category({Keys.SubCategory.parent_category_id: 1,
                                  Keys.SubCategory.name: "subcat1",
                                  Keys.SubCategory.plan: "plan1"}, self.session)
        manager.add_sub_category({Keys.SubCategory.parent_category_id: 2,
                                  Keys.SubCategory.name: "subcat2",
                                  Keys.SubCategory.plan: "plan2"}, self.session)
        sub_name = manager.get_sub_category_name_by_id(self.session, 2)
        self.assertEqual("subcat2", sub_name)
    def testGetSubCategories(self):
        manager = data_manager.DataManager()
        manager.add_parent_category({Keys.ParentCategory.name: "cat1"}, self.session)
        manager.add_parent_category({Keys.ParentCategory.name: "cat2"}, self.session)
        manager.add_sub_category({Keys.SubCategory.parent_category_id: 1,
                                  Keys.SubCategory.name: "subcat1",
                                  Keys.SubCategory.plan: "plan1"}, self.session)
        manager.add_sub_category({Keys.SubCategory.parent_category_id: 2,
                                  Keys.SubCategory.name: "subcat2",
                                  Keys.SubCategory.plan: "plan2"}, self.session)
        sub_cats = manager.get_sub_categories(self.session)
        self.assertEqual(sub_cats[0][Keys.SubCategory.name], "subcat1")        


        

        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()