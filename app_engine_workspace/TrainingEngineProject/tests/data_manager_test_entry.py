'''
Created on Jun 24, 2014

@author: Matthew
'''
'''
Make sure you have an empty database for unit testing
Create the engine, create one connection and start a transaction in it
Create the tables
Optionally, insert test fixtures
For every test, repeat:
    Create a savepoint
    Run the test
    Roll back to the savepoint
Roll back the transaction
'''
import unittest
from service import BaseService
import models
from backend_keys import Keys
import data_manager
import datetime

class Test(unittest.TestCase):
    
    def setUp(self):
        global transaction, connection, engine
        # Connect to the database and create the schema within a transaction
        models.create_tables(BaseService.engine_unit_test)
        self.session = BaseService.Session_unit_test()
        self.session.begin_nested()
        
    def tearDown(self):
        self.session.rollback()
        models.drop_all_tables(BaseService.engine_unit_test)
        self.session.close()
    
    def testAddOneEntry(self):
        manager = data_manager.DataManager()
        manager.add_parent_category({Keys.ParentCategory.name: "parent1"}, self.session)
        manager.add_sub_category({Keys.SubCategory.name: "sub1",
                                  Keys.SubCategory.parent_category_id: 1}, self.session)
        dog = self.get_dummy_dogs(1)[0]
        user = self.get_dummy_users(1)[0]
        manager.add_dog(dog, self.session)
        manager.add_user(user, self.session)
        entry = {Keys.Entry.dog_id: 1,
                 Keys.Entry.plan: "plan1",
                 Keys.Entry.sub_category_id: 1,
                 Keys.Entry.session_date: datetime.date(2012,1,1),
                 Keys.Entry.trials_result: "10111",
                 Keys.Entry.user_id: 1}
        manager.add_entry(entry, self.session)
        entry = manager.get_entries(self.session)[0]
        self.assertEqual(entry[Keys.Entry.plan], "plan1")
        self.assertEqual(entry[Keys.Entry.trials_result], "10111")
        self.assertEqual(manager.get_num_entries(self.session), 1)
        
        
        
    def get_dummy_dogs(self, num_dogs):
        kv_pairs1 = {Keys.Dog.name: "name1", 
                    Keys.Dog.birth_date: datetime.date(2001, 1, 1),
                    Keys.Dog.breed: "breed1",
                    Keys.Dog.service_type: "service1",
                    Keys.Dog.image: "image1"}
        if num_dogs == 1:
            return [kv_pairs1]
        kv_pairs2 = {Keys.Dog.name: "name2", 
                    Keys.Dog.birth_date: datetime.date(2002, 2, 2),
                    Keys.Dog.breed: "breed2",
                    Keys.Dog.service_type: "service2",
                    Keys.Dog.image: "image2"}
        if num_dogs == 2:
            return [kv_pairs1, kv_pairs2]
        kv_pairs3 = {Keys.Dog.name: "name3", 
                    Keys.Dog.birth_date: datetime.date(2003, 3, 3),
                    Keys.Dog.breed: "breed3",
                    Keys.Dog.service_type: "service3",
                    Keys.Dog.image: "image3"}
        if num_dogs == 3:
            return [kv_pairs1, kv_pairs2, kv_pairs3]
    def get_dummy_users(self, num_users):
        kv_pairs1 = {Keys.User.email: "email1", 
                     Keys.User.full_name: "fullname1",
                     Keys.User.password: "password1",
                     Keys.User.phone: "phone1"}
        if num_users == 1:
            return [kv_pairs1]
        kv_pairs2 = {Keys.User.email: "email2", 
                     Keys.User.full_name: "fullname2",
                     Keys.User.password: "password2",
                     Keys.User.phone: "phone2"}
        if num_users == 2:
            return [kv_pairs1, kv_pairs2]
        kv_pairs3 = {Keys.User.email: "email3", 
                     Keys.User.full_name: "fullname3",
                     Keys.User.password: "password3",
                     Keys.User.phone: "phone3"}
        if num_users == 3:
            return [kv_pairs1, kv_pairs2, kv_pairs3]
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()