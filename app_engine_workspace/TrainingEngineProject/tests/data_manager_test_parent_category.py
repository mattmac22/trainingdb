'''
Created on Jun 24, 2014

@author: Matthew
'''
'''
Make sure you have an empty database for unit testing
Create the engine, create one connection and start a transaction in it
Create the tables
Optionally, insert test fixtures
For every test, repeat:
    Create a savepoint
    Run the test
    Roll back to the savepoint
Roll back the transaction
'''
import unittest
from service import BaseService
import models
from backend_keys import Keys
import data_manager
import datetime
import custom_exceptions

class Test(unittest.TestCase):
    
    def setUp(self):
        global transaction, connection, engine
        # Connect to the database and create the schema within a transaction
        models.create_tables(BaseService.engine_unit_test)
        self.session = BaseService.Session_unit_test()
        self.session.begin_nested()
        
    def tearDown(self):
        self.session.rollback()
        models.drop_all_tables(BaseService.engine_unit_test)
        self.session.close()
    
    def testAddOneParentCategory(self):
        manager = data_manager.DataManager()
        manager.add_parent_category({Keys.ParentCategory.name: "cat1"}, self.session)
        cats = manager.get_parent_categories(self.session, {})
        self.assertTrue(manager.contains_parent_category(self.session, {Keys.ParentCategory.name: "cat1"}))
        self.assertTrue(manager.contains_parent_category_by_name(self.session, "cat1"))
        self.assertFalse(manager.contains_parent_category_by_name(self.session, "cat2"))
        self.assertEqual(cats[0][Keys.ParentCategory.name], "cat1")
    def testAddTwoParentCategories(self):
        manager = data_manager.DataManager()
        manager.add_parent_category({Keys.ParentCategory.name: "cat1"}, self.session)
        manager.add_parent_category({Keys.ParentCategory.name: "cat2"}, self.session)
        cats = manager.get_parent_categories(self.session, {})
        self.assertEqual(cats[0][Keys.ParentCategory.name], "cat1")
        self.assertEqual(cats[1][Keys.ParentCategory.name], "cat2")
    def testAddDuplicateCategory(self):
        manager = data_manager.DataManager()
        manager.add_parent_category({Keys.ParentCategory.name: "cat1"}, self.session)
        self.assertRaises(custom_exceptions.DuplicateEntry, manager.add_parent_category, {Keys.ParentCategory.name: "cat1"}, self.session)
        self.assertRaises(custom_exceptions.DuplicateEntry, manager.add_parent_category, {Keys.ParentCategory.name: "Cat1"}, self.session)
    def testToggleVisibility(self):
        manager = data_manager.DataManager()
        manager.add_parent_category({Keys.ParentCategory.name: "cat1"}, self.session)
        manager.add_parent_category({Keys.ParentCategory.name: "cat2"}, self.session)
        manager.change_parent_category_visibility(self.session, 1, False)
        cats = manager.get_parent_categories(self.session, {})
        self.assertEqual(cats[0][Keys.ParentCategory.name], "cat2")
        self.assertEqual(len(cats), 1)
    def testForCatByName(self):
        manager = data_manager.DataManager()
        manager.add_parent_category({Keys.ParentCategory.name: "catEGory1"}, self.session)
        self.assertTrue(manager.contains_parent_category_by_name(self.session, "category1"))
    def testGetParentCategoryNameByID(self):
        manager = data_manager.DataManager()
        manager.add_parent_category({Keys.ParentCategory.name: "cat1"}, self.session)
        manager.add_parent_category({Keys.ParentCategory.name: "cat2"}, self.session)
        category_name = manager.get_parent_category_name_by_id(self.session, 2)
        self.assertEqual("cat2", category_name)

    
        

        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()