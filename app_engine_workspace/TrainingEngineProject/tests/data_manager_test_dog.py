'''
Created on Jun 24, 2014

@author: Matthew
'''
'''
Make sure you have an empty database for unit testing
Create the engine, create one connection and start a transaction in it
Create the tables
Optionally, insert test fixtures
For every test, repeat:
    Create a savepoint
    Run the test
    Roll back to the savepoint
Roll back the transaction
'''
import unittest
from service import BaseService
import models
from backend_keys import Keys
import data_manager
import datetime

class Test(unittest.TestCase):
    
    def setUp(self):
        global transaction, connection, engine
        # Connect to the database and create the schema within a transaction
        models.create_tables(BaseService.engine_unit_test)
        self.session = BaseService.Session_unit_test()
        self.session.begin_nested()
        
    def tearDown(self):
        self.session.rollback()
        models.drop_all_tables(BaseService.engine_unit_test)
        self.session.close()
    
    def testAddOneDog(self):
        manager = data_manager.DataManager()
        kv_pairs = self.get_dummy_dogs(1)[0]
        manager.add_dog(kv_pairs, self.session)
        dogs = manager.get_dogs(self.session)
        assert(dogs[0][Keys.Dog.name] == "name1")
        assert(dogs[0][Keys.Dog.breed] == "breed1")
        self.session.commit()
    def testAddNoDogs(self):
        manager = data_manager.DataManager()
        assert(manager.get_num_dogs(self.session) == 0)
        self.session.commit()
    def testAddThreeDogs(self):
        manager = data_manager.DataManager()
        dogs = self.get_dummy_dogs(3)
        manager.add_dog(dogs[0], self.session)
        manager.add_dog(dogs[1], self.session)
        manager.add_dog(dogs[2], self.session)
        assert(manager.get_num_dogs(self.session) == 3)
        dogs = manager.get_dogs(self.session)
        assert(dogs[0][Keys.Dog.name] == "name1")
        assert(dogs[1][Keys.Dog.name] == "name2")
        assert(dogs[2][Keys.Dog.name] == "name3")
        self.session.commit()
    def get_dummy_dogs(self, num_dogs):
        kv_pairs1 = {Keys.Dog.name: "name1", 
                    Keys.Dog.birth_date: datetime.date(2001, 1, 1),
                    Keys.Dog.breed: "breed1",
                    Keys.Dog.service_type: "service1",
                    Keys.Dog.image: "image1"}
        if num_dogs == 1:
            return [kv_pairs1]
        kv_pairs2 = {Keys.Dog.name: "name2", 
                    Keys.Dog.birth_date: datetime.date(2002, 2, 2),
                    Keys.Dog.breed: "breed2",
                    Keys.Dog.service_type: "service2",
                    Keys.Dog.image: "image2"}
        if num_dogs == 2:
            return [kv_pairs1, kv_pairs2]
        kv_pairs3 = {Keys.Dog.name: "name3", 
                    Keys.Dog.birth_date: datetime.date(2003, 3, 3),
                    Keys.Dog.breed: "breed3",
                    Keys.Dog.service_type: "service3",
                    Keys.Dog.image: "image3"}
        if num_dogs == 3:
            return [kv_pairs1, kv_pairs2, kv_pairs3]
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()