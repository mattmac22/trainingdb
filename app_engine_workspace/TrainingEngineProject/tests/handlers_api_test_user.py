'''
Created on Aug 15, 2014

@author: Matthew
'''
import unittest
from service import BaseService
import models
from backend_keys import Keys
import data_manager
import datetime
import custom_exceptions
import admin_manager
from google.appengine.ext import testbed
import handlers_api
import webapp2
import webtest
import json
import main

class Test(unittest.TestCase):
    
    def setUp(self):
        global transaction, connection, engine
        routes = [
            (r'/api/addUser',handlers_api.AddUser),
            (r'/api/getUsers',handlers_api.GetUsers)
        ]
        app = webapp2.WSGIApplication(routes)
        self.testapp = webtest.TestApp(app)
        # Connect to the database and create the schema within a transaction
        models.create_tables(BaseService.engine_unit_test)
        self.session = BaseService.Session_unit_test()
        self.session.begin_nested()
        self.testbed = testbed.Testbed()
        # USER AUTHENTICATION STUFF
        self.testbed.setup_env(USER_EMAIL="admin@gmail.com",USER_ID='1')
        # Then activate the testbed, which prepares the service stubs for use.
        self.testbed.activate()
        
    def tearDown(self):
        self.session.rollback()
        models.drop_all_tables(BaseService.engine_unit_test)
        self.session.close()
    def testAddOne(self):
        response = self.testapp.post("/api/getUsers",{"last_updated": "2014-08-16 02:30:00"})
        print response.body
        '''
        user = {Keys.User.email: "email@gmail.com", 
                Keys.User.full_name: "John Doe",
                Keys.User.password: "password",
                Keys.User.phone: "Phone",
                Keys.User.user_name: "User Name" }
        
        response = self.testapp.post("/api/addUser", user)
        print response
        manager = data_manager.DataManager()
        users = manager.get_users(self.session)
        print len(users)
        assert(users[0][Keys.User.email] == "email@gmail.com")
        '''
        
        
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
    
    
    
    