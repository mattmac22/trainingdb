'''
Created on Jun 24, 2014

@author: Matthew
'''
'''
Make sure you have an empty database for unit testing
Create the engine, create one connection and start a transaction in it
Create the tables
Optionally, insert test fixtures
For every test, repeat:
    Create a savepoint
    Run the test
    Roll back to the savepoint
Roll back the transaction
'''
import unittest
from service import BaseService
import models
from backend_keys import Keys
import data_manager
import datetime
import input_parser
import json
from StringIO import StringIO
import custom_exceptions

class Test(unittest.TestCase):
    
    def setUp(self):
        pass
        
    def tearDown(self):
        pass
    
    def testLinkUnique(self):
        parser = input_parser.PlanParser()
        self.assertEqual(parser.list_unique([1,2,3,1]), False)
        self.assertEqual(parser.list_unique(["one", "two", "three"]),True)
    def testParse(self):
        parser = input_parser.PlanParser()
        plan = [{"name": "question1",
                 "answers": ["Answer Number One", "Answer Number Two", "Answer Number 3"]},
                {"name": "question2",
                 "answers": ["Answer Number One", "Answer Number Two", "Answer Number 3"]}]
        parser.validate_plan(plan)
    def testParseRaisesError(self):
        parser = input_parser.PlanParser()
        plan = [{"name": "question1",
                 "answers": ["Answer Number One", "Answer Number Two", "Answer Number 3"]},
                {"name": "question1",
                 "answers": ["Answer Number One", "Answer Number Two", "Answer Number 3"]}]
        self.assertRaises(custom_exceptions.ListNotUnique, parser.validate_plan, plan)
    def testParseRaisesError2(self):
        parser = input_parser.PlanParser()
        plan = [{"name": "question1",
                 "answers": ["Answer Number One", "Answer Number One", "Answer Number 3"]},
                {"name": "question2",
                 "answers": ["Answer Number One", "Answer Number Two", "Answer Number 3"]}]
        self.assertRaises(custom_exceptions.ListNotUnique, parser.validate_plan, plan)
        
        
        
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()