'''
Created on Jun 21, 2014

@author: Matthew
'''
import custom_exceptions
from backend_keys import Keys
import data_manager
import admin_manager

class UserValidator():
    def validate_kv_pairs(self, kv_pairs):
        allRequiredFieldsPresent(kv_pairs, Keys.User.FIELDS_REQUIRED)
        allFieldsValid(kv_pairs, Keys.User.FIELDS_ALL)
class CredentialValidator():
    def __init__(self, session):
        self.session = session
    def validate_kv_pairs(self, kv_pairs):
        allRequiredFieldsPresent(kv_pairs, Keys.Credential.FIELDS_REQUIRED)
        allFieldsValid(kv_pairs, Keys.Credential.FIELDS_ALL)
        self.__credential_unique(kv_pairs)
    def __credential_unique(self, kv_pairs):
        manager = admin_manager.AdminManager()
        contains_credential = manager.contains_credential(kv_pairs[Keys.Credential.email], self.session)
        if contains_credential:
            raise custom_exceptions.DuplicateEntry("Credential already contained: " + kv_pairs[Keys.Credential.email])
class EntryValidator():
    def validate_kv_pairs(self, kv_pairs):
        allRequiredFieldsPresent(kv_pairs, Keys.Entry.FIELDS_REQUIRED)
        allFieldsValid(kv_pairs, Keys.Entry.FIELDS_ALL)
class DogValidator():
    def validate_kv_pairs(self, kv_pairs):
        allRequiredFieldsPresent(kv_pairs, Keys.Dog.FIELDS_REQUIRED)
        allFieldsValid(kv_pairs, Keys.Dog.FIELDS_ALL)
class ParentCategoryValidator():
    def __init__(self, session):
        self.session = session
    def validate_kv_pairs(self, kv_pairs):
        allRequiredFieldsPresent(kv_pairs, Keys.ParentCategory.FIELDS_REQUIRED)
        allFieldsValid(kv_pairs, Keys.ParentCategory.FIELDS_ALL)
        self.__category_unique(kv_pairs)
    def __category_unique(self, kv_pairs):
        manager = data_manager.DataManager()
        contains_category = manager.contains_parent_category_by_name(self.session, kv_pairs[Keys.ParentCategory.name])
        if contains_category:
            raise custom_exceptions.DuplicateEntry(kv_pairs[Keys.ParentCategory.name])
class SubCategoryValidator():
    def validate_kv_pairs(self, kv_pairs):
        allRequiredFieldsPresent(kv_pairs, Keys.SubCategory.FIELDS_REQUIRED)
        allFieldsValid(kv_pairs, Keys.SubCategory.FIELDS_ALL)
    
def allRequiredFieldsPresent(kv_pairs, FIELDS_REQUIRED):
    for field in FIELDS_REQUIRED:
        if field not in kv_pairs:
            raise custom_exceptions.MissingField('Key values missing required field: ' + field)
def allFieldsValid(kv_pairs, FIELDS_ALL):
    for key in kv_pairs:
        if not key in FIELDS_ALL:
            raise custom_exceptions.InvalidField('Invalid field: ' + key)
    