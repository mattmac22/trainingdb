'''
Created on Aug 27, 2014

@author: Matthew
'''

from google.appengine.ext.remote_api import remote_api_stub
import getpass
import data_manager
from backend_keys import Keys
import service
import json

def auth_func():
  return ('matthewtmaclean@gmail.com', getpass.getpass('Password:'))

remote_api_stub.ConfigureRemoteApi(None, '/_ah/remote_api', auth_func,
                                   'localhost:7777')


# Parent Categories
kv_pairs = {Keys}
def add_parent_category(session, manager, name):
    print "Adding parent category: " + name
    kv_pairs = {Keys.ParentCategory.name: name}
    manager.add_parent_category(kv_pairs, session)
    
session = service.BaseService.Session()
manager = data_manager.DataManager()
add_parent_category(session, manager, "Agility")
add_parent_category(session, manager, "Direction and Control")
add_parent_category(session, manager, "Foundational Obedience")
add_parent_category(session, manager, "Search")


def add_sub_category(session, manager, name, parent_category_id, plan):
    print "Adding subcategory: " + name
    kv_pairs = {Keys.SubCategory.name: name, Keys.SubCategory.plan: plan, Keys.SubCategory.parent_category_id: parent_category_id}
    manager.add_sub_category(kv_pairs, session)
def add_question_to_list(list, name, type, answers=[]):
    kv_pair = {"name": name, "type": type}
    if answers != []:
        kv_pair["answers"] = answers
    list.append(kv_pair)
        

# AG-Balance
question_list = []
add_question_to_list(question_list, "Handler Position", 's', ["None", "Next to K9", "From a distance"])
add_question_to_list(question_list, "Obstacle", 's', ["None", "Cart or Scooter", "Rolling Plank", "Swaying Bridge", "Teeter Totter", "Wobbly Plank", "Unstable Surface", "Moving Surface", "Other"])
add_question_to_list(question_list, "Obstacle Height", 's', ["None", "0-2'","3-4'","5-6'","7-8'"])
add_question_to_list(question_list, "Direction", 's', ["None","Up","Down"])
add_question_to_list(question_list, "Hand Signal", 'c')
add_question_to_list(question_list, "Command", 'c')
add_question_to_list(question_list, "Other Dogs", "s", ["None","S","M"])
add_question_to_list(question_list, "Humans", "s", ["None","S","M"])
add_question_to_list(question_list, "Toys", "s", ["None","S","M"])
add_question_to_list(question_list, "Food", "c")
add_question_to_list(question_list, "Noise", "c")
add_question_to_list(question_list, "Off Lead", "c")
add_question_to_list(question_list, "Result", "s", ["None","Y","N"])
add_sub_category(session, manager, "Balance", 1, json.dumps(question_list))

#AG - Climb
question_list = []
add_question_to_list(question_list, "Handler Position", 's', ["None", "Next to K9", "From a distance"])
add_question_to_list(question_list, "Obstacle", 's', ["None", "Pallet","Wooden Ladder","Metal Ladder", "Wicket Walk", "16-18\" on Center Frame", "Open Stairs", "Plank", "Invented"])
add_question_to_list(question_list, "Degree of Incline", "s", ["None", u'0-15\N{DEGREE SIGN}', u'15-30\N{DEGREE SIGN}', u'30-45\N{DEGREE SIGN}'])
add_question_to_list(question_list, "Direction", 's', ["None","Up","Down"])
add_question_to_list(question_list, "Hand Signal", 'c')
add_question_to_list(question_list, "Command", 'c')
add_question_to_list(question_list, "Other Dogs", "s", ["None","S","M"])
add_question_to_list(question_list, "Humans", "s", ["None","S","M"])
add_question_to_list(question_list, "Toys", "s", ["None","S","M"])
add_question_to_list(question_list, "Food", "c")
add_question_to_list(question_list, "Noise", "c")
add_question_to_list(question_list, "Off Lead", "c")
add_question_to_list(question_list, "Result", "s", ["None","Y","N"])
add_sub_category(session, manager, "Climb", 1, json.dumps(question_list))


#AG - Crawl
question_list=[]
add_question_to_list(question_list, "Handler Position", 's', ["None", "Next to K9", "From a distance"])
add_question_to_list(question_list, "Obstacle Material", 's', ["Flexible Netting", "Open Grate", "Solid","Slippery","Uneven","Invented"])
add_question_to_list(question_list, "Light", 's', ["Visible Light", "Total Darkness"])
add_question_to_list(question_list, "Distance", 's', ["None", "3-6'", "6-15'", ">15'"])
add_question_to_list(question_list, "Direction", 's', ["None","Forward","Backward","Other"])
add_question_to_list(question_list, "Hand Signal", 'c')
add_question_to_list(question_list, "Command", 'c')
add_question_to_list(question_list, "Other Dogs", "s", ["None","S","M"])
add_question_to_list(question_list, "Humans", "s", ["None","S","M"])
add_question_to_list(question_list, "Toys", "s", ["None","S","M"])
add_question_to_list(question_list, "Food", "c")
add_question_to_list(question_list, "Noise", "c")
add_question_to_list(question_list, "Off Lead", "c")
add_question_to_list(question_list, "Result", "s", ["None","Y","N"])
add_sub_category(session, manager, "Crawl", 1, json.dumps(question_list))

#AG - Tunnel
question_list=[]
add_question_to_list(question_list, "Handler Position", 's', ["None", "Next to K9", "From a distance"])
add_question_to_list(question_list, "Direction", 's', ["None","Forward","Backward","Other"])
add_question_to_list(question_list, "Distance", 's', ["None", "3-6'", "6-15'", ">15'"])
add_question_to_list(question_list, "Tunnel", 's', ["Straight", "90\N{DEGREE SIGN} Turn", "Covered End", "Debris Inside", "Elevated", "Slippery", "Moving Tunnel", "Invented", "Dark Tunnel", "Inclined Angle"])
add_question_to_list(question_list, "Hand Signal", 'c')
add_question_to_list(question_list, "Command", 'c')
add_question_to_list(question_list, "Other Dogs", "s", ["None","S","M"])
add_question_to_list(question_list, "Humans", "s", ["None","S","M"])
add_question_to_list(question_list, "Toys", "s", ["None","S","M"])
add_question_to_list(question_list, "Food", "c")
add_question_to_list(question_list, "Noise", "c")
add_question_to_list(question_list, "Off Lead", "c")
add_question_to_list(question_list, "Result", "s", ["None","Y","N"])
add_sub_category(session, manager, "Tunnel", 1, json.dumps(question_list))

#AG - Walk it Plank
question_list=[]
add_question_to_list(question_list, "Handler Position", 's', ["None", "Next to K9", "From a distance"])
add_question_to_list(question_list, "Plank Height", 's', ["None", "0-2'", "3-4'", "5-6'", "7-9'", ">9'"])
add_question_to_list(question_list, "Plank Material", 's', ["None", "Straight (12-18\" W)", "1-8' Length", ">9' Length", "Stable Plank", "2(2X4) Boards", "Plank with Debris", "Moving Planks", "4-6\" Width"])
add_question_to_list(question_list, "Hand Signal", 'c')
add_question_to_list(question_list, "Command", 'c')
add_question_to_list(question_list, "Other Dogs", "s", ["None","S","M"])
add_question_to_list(question_list, "Humans", "s", ["None","S","M"])
add_question_to_list(question_list, "Toys", "s", ["None","S","M"])
add_question_to_list(question_list, "Food", "c")
add_question_to_list(question_list, "Noise", "c")
add_question_to_list(question_list, "Off Lead", "c")
add_question_to_list(question_list, "Result", "s", ["None","Y","N"])
add_sub_category(session, manager, "Walk it Plank", 1, json.dumps(question_list))

#AG - Walk It Surface
question_list=[]
add_question_to_list(question_list, "Handler Position", 's', ["None", "Next to K9", "From a distance"])
add_question_to_list(question_list, "Obstacle", 's', ["None", "Horizontal", "Inclined","Elevated","Mvoing","0-8' Length", ">8' Length", "With Debris"])
add_question_to_list(question_list, "Surface", 's', ["None","Tile","Metal","Carpet","Plastic","Concrete","Wood","Invented"])
add_question_to_list(question_list, "Hand Signal", 'c')
add_question_to_list(question_list, "Command", 'c')
add_question_to_list(question_list, "Other Dogs", "s", ["None","S","M"])
add_question_to_list(question_list, "Humans", "s", ["None","S","M"])
add_question_to_list(question_list, "Toys", "s", ["None","S","M"])
add_question_to_list(question_list, "Food", "c")
add_question_to_list(question_list, "Noise", "c")
add_question_to_list(question_list, "Off Lead", "c")
add_question_to_list(question_list, "Result", "s", ["None","Y","N"])
add_sub_category(session, manager, "Walk it Surface", 1, json.dumps(question_list))

# DAC - Back
question_list = []
add_question_to_list(question_list,"K9 Position", 's',["None","From Sit","From Stand","From Down","Ground Level", "From Object"])
add_question_to_list(question_list,"Handler Radius", 's',["None","0-3'", "4-10'", "11-20'", "21-75'", "76-100'"])
add_question_to_list(question_list,"Back Distance", 's',["None","5-10'", "11-15'", "16-20'", "21-75'", "76-100'"])
add_question_to_list(question_list,"Object", 's',["None","To Elevated Target < 1'", "To Elevated Target > 1-3'", "To Real World Target (boulders, bench, hay bale)"])
add_question_to_list(question_list, "Other Dogs", "s", ["None","S","M"])
add_question_to_list(question_list, "Humans", "s", ["None","S","M"])
add_question_to_list(question_list, "Toys", "s", ["None","S","M"])
add_question_to_list(question_list, "Food", "c")
add_question_to_list(question_list, "Noise", "c")
add_question_to_list(question_list, "Off Lead", "c")
add_question_to_list(question_list, "Result", "s", ["None","Y","N"])
add_sub_category(session, manager, "Back", 2, json.dumps(question_list))

# DAC - Go Out
question_list = []
add_question_to_list(question_list,"K9 Position", 's',["None","From Sit","From Stand","From Heel"])
add_question_to_list(question_list,"Handler Radius", 's',["None","5-10'", "11-15'", "16-20'", "21-75'", "76-100'"])
add_question_to_list(question_list,"Object", 's',["None","To Elevated Target < 1'", "To Elevated Target > 1-3'", "To Ground Level Target", "To Real World Target (boulders, bench, hay bale)"])
add_question_to_list(question_list,"Go Out Distance", 's',["None","5-10'", "11-15'", "16-20'", "21-75'", "76-100'"])
add_question_to_list(question_list,"Sequence", 's',["Second", "First and Third","Second and Third", "First and Second", "First, Second, and Third"])
add_question_to_list(question_list,"Sequence", 's',[])
add_question_to_list(question_list, "Other Dogs", "s", ["None","S","M"])
add_question_to_list(question_list, "Humans", "s", ["None","S","M"])
add_question_to_list(question_list, "Toys", "s", ["None","S","M"])
add_question_to_list(question_list, "Food", "c")
add_question_to_list(question_list, "Noise", "c")
add_question_to_list(question_list, "Off Lead", "c")
add_question_to_list(question_list, "Result", "s", ["None","Y","N"])
add_sub_category(session, manager, "Go Out", 2, json.dumps(question_list))

# DAC - Hup and Off
question_list = []
add_question_to_list(question_list, "K9 Distance", "s", ["None","Spool","New Item"])
add_question_to_list(question_list, "Handler Radius", "s", ["0-3'","4-10'", "11-20'",">20'"])
add_question_to_list(question_list, "Duration (for STAY)", "s", ["None","0-5 s","6-19 s", "20-44 s", "45-60 s", "1-2 min", "2-3 min", "3-5 min", ">5 min"])
add_question_to_list(question_list, "Height", "s", ["None","0-12\"", "13-24\"", "25-36\"",">36\""])
add_question_to_list(question_list, "Object", "s", ["None","Spool","Table","Pallet","Real World","Plastic","Metal","Concrete"])
add_question_to_list(question_list, "Other Dogs", "s", ["None","S","M"])
add_question_to_list(question_list, "Humans", "s", ["None","S","M"])
add_question_to_list(question_list, "Toys", "s", ["None","S","M"])
add_question_to_list(question_list, "Food", "c")
add_question_to_list(question_list, "Noise", "c")
add_question_to_list(question_list, "Off Lead", "c")
add_question_to_list(question_list, "Result", "s", ["None","Y","N"])
add_sub_category(session, manager, "Hup and Off", 2, json.dumps(question_list))

# DAC - Over
question_list = []
add_question_to_list(question_list,"K9 Position", 's',["None","From Sit","From Stand","From Down","Ground Level", "From Object"])
add_question_to_list(question_list, "Handler Radius", "s", ["0-3'","4-10'", "11-20'","21-75'","76-100'"])
add_question_to_list(question_list,"Object", 's',["None","To Elevated Target < 1'", "To Elevated Target > 1-3'", "To Real World Target (boulders, bench, hay bale)"])
add_question_to_list(question_list, "Distance of Travel Left", 'c')
add_question_to_list(question_list, "Distance of Travel Right", 'c')
add_question_to_list(question_list, "Other Dogs", "s", ["None","S","M"])
add_question_to_list(question_list, "Humans", "s", ["None","S","M"])
add_question_to_list(question_list, "Toys", "s", ["None","S","M"])
add_question_to_list(question_list, "Food", "c")
add_question_to_list(question_list, "Noise", "c")
add_question_to_list(question_list, "Off Lead", "c")
add_question_to_list(question_list, "Result", "s", ["None","Y","N"])
add_sub_category(session, manager, "Over", 2, json.dumps(question_list))

# DAC - Patterns
question_list = []
add_question_to_list(question_list, "Pattern", "s", ["None","Home Plate","Pitchers Mound","First Base","Second Base","Third Base"])
add_question_to_list(question_list, "Handler Radius", "s", ["0-3'","4-10'", "11-20'","21-75'","76-100'"])
add_question_to_list(question_list, "Object Spacing", "s", ["0-3'","4-10'", "11-20'","21-75'","76-100'"])
add_question_to_list(question_list,"Object", 's',["None","To Elevated Target < 1'", "To Elevated Target > 1-3'", "To Real World Target (boulders, bench, hay bale)"])
add_question_to_list(question_list, "Other Dogs", "s", ["None","S","M"])
add_question_to_list(question_list, "Humans", "s", ["None","S","M"])
add_question_to_list(question_list, "Toys", "s", ["None","S","M"])
add_question_to_list(question_list, "Food", "c")
add_question_to_list(question_list, "Noise", "c")
add_question_to_list(question_list, "Off Lead", "c")
add_question_to_list(question_list, "Result", "s", ["None","Y","N"])
add_sub_category(session, manager, "Patterns", 2, json.dumps(question_list))

# FO - Come-Recall
question_list = []
add_question_to_list(question_list, "K9 Position", "s", ["None","Ground","Unstable Surface","Elevated","In Motion"])
add_question_to_list(question_list, "Handler Radius", "s", ["None","0-3'","4-10'","11-20'",">20'"])
add_question_to_list(question_list, "Duration (for STAY)", "s", ["None","0-5 s","6-19 s", "20-44 s", "45-60 s", "1-2 min", "2-3 min", "3-5 min", ">5 min"])
add_question_to_list(question_list, "Other Dogs", "s", ["None","S","M"])
add_question_to_list(question_list, "Humans", "s", ["None","S","M"])
add_question_to_list(question_list, "Toys", "s", ["None","S","M"])
add_question_to_list(question_list, "Food", "c")
add_question_to_list(question_list, "Noise", "c")
add_question_to_list(question_list, "Off Lead", "c")
add_question_to_list(question_list, "Result", "s", ["None","Y","N"])
add_sub_category(session, manager, "Come-Recall", 3, json.dumps(question_list))

# FO - Down
question_list = []
add_question_to_list(question_list, "K9 Position", "s", ["None","Ground","Unstable Surface","Elevated","In Motion"])
add_question_to_list(question_list, "Handler Radius", "s", ["None","0-3'","4-10'","11-20'",">20'"])
add_question_to_list(question_list, "Duration (for STAY)", "s", ["None","0-5 s","6-19 s", "20-44 s", "45-60 s", "1-2 min", "2-3 min", "3-5 min", ">5 min"])
add_question_to_list(question_list, "Other Dogs", "s", ["None","S","M"])
add_question_to_list(question_list, "Humans", "s", ["None","S","M"])
add_question_to_list(question_list, "Toys", "s", ["None","S","M"])
add_question_to_list(question_list, "Food", "c")
add_question_to_list(question_list, "Noise", "c")
add_question_to_list(question_list, "Off Lead", "c")
add_question_to_list(question_list, "Result", "s", ["None","Y","N"])
add_sub_category(session, manager, "Down", 3, json.dumps(question_list))

# FO - Stay
question_list = []
add_question_to_list(question_list, "K9 Position", "s", ["None","Ground","Unstable Surface","Elevated","In Motion"])
add_question_to_list(question_list, "Handler Radius", "s", ["None","0-3'","4-10'","11-20'",">20'"])
add_question_to_list(question_list, "Duration (for STAY)", "s", ["None","0-5 s","6-19 s", "20-44 s", "45-60 s", "1-2 min", "2-3 min", "3-5 min", ">5 min"])
add_question_to_list(question_list, "Other Dogs", "s", ["None","S","M"])
add_question_to_list(question_list, "Humans", "s", ["None","S","M"])
add_question_to_list(question_list, "Toys", "s", ["None","S","M"])
add_question_to_list(question_list, "Food", "c")
add_question_to_list(question_list, "Noise", "c")
add_question_to_list(question_list, "Off Lead", "c")
add_question_to_list(question_list, "Result", "s", ["None","Y","N"])
add_sub_category(session, manager, "Down-Stay", 3, json.dumps(question_list))

# FO - Heel
question_list = []
add_question_to_list(question_list, "Degree of Difficulty", "s", ["None","Standing Heel","2-5 Paces Walking","20 Yard Walk", "Fast Paced 0-10 Yards","Fast Paced > 10 Yards",u'90\N{DEGREE SIGN} Turn Walk',u'90\N{DEGREE SIGN} Turn Fast','Slow',"About Turn","Varied Pattern"])
add_question_to_list(question_list, "Other Dogs", "s", ["None","S","M"])
add_question_to_list(question_list, "Humans", "s", ["None","S","M"])
add_question_to_list(question_list, "Toys", "s", ["None","S","M"])
add_question_to_list(question_list, "Food", "c")
add_question_to_list(question_list, "Noise", "c")
add_question_to_list(question_list, "Off Lead", "c")
add_question_to_list(question_list, "Result", "s", ["None","Y","N"])
add_sub_category(session, manager, "Heel", 3, json.dumps(question_list))

# FO - Sit-Stay
question_list = []
add_question_to_list(question_list, "K9 Position", "s", ["None","Ground","Unstable Surface","Elevated","In Motion"])
add_question_to_list(question_list, "Handler Radius", "s", ["None","0-3'","4-10'","11-20'",">20'"])
add_question_to_list(question_list, "Duration (for STAY)", "s", ["None","0-5 s","6-19 s", "20-44 s", "45-60 s", "1-2 min", "2-3 min", "3-5 min", ">5 min"])
add_question_to_list(question_list, "Other Dogs", "s", ["None","S","M"])
add_question_to_list(question_list, "Humans", "s", ["None","S","M"])
add_question_to_list(question_list, "Toys", "s", ["None","S","M"])
add_question_to_list(question_list, "Food", "c")
add_question_to_list(question_list, "Noise", "c")
add_question_to_list(question_list, "Off Lead", "c")
add_question_to_list(question_list, "Result", "s", ["None","Y","N"])
add_sub_category(session, manager, "Sit-Stay", 3, json.dumps(question_list))

# FO - Stay
question_list = []
add_question_to_list(question_list, "K9 Position", "s", ["None","Ground","Unstable Surface","Elevated","In Motion"])
add_question_to_list(question_list, "Handler Radius", "s", ["None","0-3'","4-10'","11-20'",">20'"])
add_question_to_list(question_list, "Duration (for STAY)", "s", ["None","0-5 s","6-19 s", "20-44 s", "45-60 s", "1-2 min", "2-3 min", "3-5 min", ">5 min"])
add_question_to_list(question_list, "Other Dogs", "s", ["None","S","M"])
add_question_to_list(question_list, "Humans", "s", ["None","S","M"])
add_question_to_list(question_list, "Toys", "s", ["None","S","M"])
add_question_to_list(question_list, "Food", "c")
add_question_to_list(question_list, "Noise", "c")
add_question_to_list(question_list, "Off Lead", "c")
add_question_to_list(question_list, "Result", "s", ["None","Y","N"])
add_sub_category(session, manager, "Stay", 3, json.dumps(question_list))

# S - Bark Alert
question_list = []
add_question_to_list(question_list, "Distance from Scent Source", "s", ["None","0-3'","4-10'","11-20'","21-30'","31-40'","41-50'","51-60'","61-70'","71-80'",">80'"])
add_question_to_list(question_list, "Hidden In", "s", ["None","Bark Barrel", "Box","Rubble","Closet","Plexiglass Box","Other"])
add_question_to_list(question_list, "Duration of Bark Alert", "s", ["None","3-5 s","6-9 s","10-13 s","14-17 s","18-21 s","22-26 s","27-30 s",">30 s"])
add_question_to_list(question_list, "Number of Barks", "s", ["None","1-5","6-10","11-15","16-20","21-25","26-30","31-35","36-40"])
add_question_to_list(question_list, "Handler Position at Time of Reward", "s", ["None","a","b > 50' from dog", "b 40-50' from dog","b 30-39' from dog", "b 20-29' from dog", "b 10-19' from dog", "b 5-9' from dog","b 1-4' from dog","Next to Dog"])
add_question_to_list(question_list, "Other Dogs", "s", ["None","S","M"])
add_question_to_list(question_list, "Humans", "s", ["None","S","M"])
add_question_to_list(question_list, "Toys", "s", ["None","S","M"])
add_question_to_list(question_list, "Food", "c")
add_question_to_list(question_list, "Noise", "c")
add_question_to_list(question_list, "Familiar Person Hidden", "c")
add_question_to_list(question_list, "Unfamiliar Person Hidden", "c")
add_question_to_list(question_list, "Victim Reward", "c")
add_question_to_list(question_list, "Handler Reward", "c")
add_question_to_list(question_list, "Result", "s", ["None","Y","N"])
add_sub_category(session, manager, "Bark Alert", 4, json.dumps(question_list))

# S -  Go Find
question_list = []
add_question_to_list(question_list, "K9", "s", ["Visual","Blind"])
add_question_to_list(question_list, "Hide Distance", "s", ["None","0-10'","11-30'","31-50'",">50'"])
add_question_to_list(question_list, "K9 Release Delay", "s", ["None","1-5 s","6-15 s","16-30 s","31-60 s","1-2 min", "2-5 min", ">5 min"])
add_question_to_list(question_list, "Hide", "s", ["Bark Barrel OPEN","Bark Barrel CLOSED","Inaccessible","Vented (high odor escape)","Sealed (low odor escape)","Buried","Elevated","Multiple Target","Blanks"])
add_question_to_list(question_list, "Other Dogs", "s", ["None","S","M"])
add_question_to_list(question_list, "Humans", "s", ["None","S","M"])
add_question_to_list(question_list, "Toys", "s", ["None","S","M"])
add_question_to_list(question_list, "Food", "c")
add_question_to_list(question_list, "Alert (Barks)","s",["None","1 Bark","2-5 Barks","5-10 Barks","11-20 Barks",">20 Barks"])
add_question_to_list(question_list, "Handler Distance (from Alert)","s",["None","0-10'","11-15'","16-30'",">30'","Out of Sight"])
add_question_to_list(question_list, "Reward","s",["None","Handler","Subject"])
add_sub_category(session, manager, "Go Find", 4, json.dumps(question_list))

# S - Search
question_list = []
add_question_to_list(question_list, "K9", "s", ["Visual","Blind"])
add_question_to_list(question_list, "Handler", "s", ["Visual","Blind"])
add_question_to_list(question_list, "Hide Distance", "s", ["None","0-10'","11-30'","31-50'",">50'"])
add_question_to_list(question_list, "K9 Release Delay", "s", ["None","1-5 s","6-15 s","16-30 s","31-60 s","1-2 min", "2-5 min", ">5 min"])
add_question_to_list(question_list, "Hide","s",["None","Scent Box/Tube","Ground/Dog Level","Inaccessible","Vented (high odor escape)", "Sealed (low odor escape)", "Buried","Elevated","Multiple Target","Blanks"])
add_question_to_list(question_list, "Other Dogs", "s", ["None","S","M"])
add_question_to_list(question_list, "Humans", "s", ["None","S","M"])
add_question_to_list(question_list, "Toys", "s", ["None","S","M"])
add_question_to_list(question_list, "Food", "c")
add_question_to_list(question_list, "Article", "s", ["None","Leather tug","Narcotics","Explosives","Human Remains","Biological/Medical"])
add_question_to_list(question_list, "Alert (Active)","s",["None","1 Bark","2-5 Barks","5-10 Barks","11-20 Barks",">20 Barks"])
add_question_to_list(question_list, "Alert (Passive)","s",["None","Sit","Down"])
add_question_to_list(question_list, "Alert (Duration)","s",["None","1-4 s","5-10 s","11-30 s", "31-60 s",">1 min"])
add_sub_category(session, manager, "Search", 4, json.dumps(question_list))

try:
    session.commit()
except Exception as e:
    session.rollback()
    print "Ended due to exception: " + str(e)




