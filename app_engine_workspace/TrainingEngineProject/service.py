'''
Created on Jun 21, 2014

@author: Matthew
'''
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

class BaseService():
    engine = create_engine('mysql://root:mag1c@localhost/trainingdb?charset=utf8&use_unicode=0', pool_recycle=3600)
    Session = sessionmaker(bind = engine)
    engine_unit_test = create_engine('mysql://root:mag1c@localhost/trainingdb_test?charset=utf8&use_unicode=0', pool_recycle=3600)
    Session_unit_test = sessionmaker(bind = engine_unit_test)