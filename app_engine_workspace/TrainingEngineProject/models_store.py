from google.appengine.ext import db
from google.appengine.api import users
from google.appengine.ext import blobstore
from backend_keys import Keys
from google.appengine.ext import blobstore

'''
Each Category has multiple entries
'''
class Category(db.Model):
    name = db.StringProperty(required=True)
    
class OptionEntry(db.Model):
    category = db.ReferenceProperty(Category,
                                   collection_name='option_entries')
    name = db.StringProperty(required=True)
    list = db.StringListProperty(required=True)

class ImageOptionEntry(db.Model):
    category = db.ReferenceProperty(Category,
                                   collection_name='image_option_entries')
    name = db.StringProperty(required=True)

class Image(db.Model):
    image_option_entry = db.ReferenceProperty(ImageOptionEntry,
                                              collection_nam='images')
    image = blobstore.BlobReferenceProperty(required=True)