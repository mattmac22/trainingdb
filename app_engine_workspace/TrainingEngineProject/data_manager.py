'''
Created on Jun 21, 2014

@author: Matthew
'''
import models
from sqlalchemy.orm import sessionmaker
import validation
from service import BaseService
from backend_keys import Keys
import custom_exceptions
from sqlalchemy import func
import cloudstorage as gcs
import gcs_utils
import base64
import image_utils
import datetime
from sqlalchemy import desc, asc

class DataManager():
    def get_category_compact(self, session, last_updated=None):
        if last_updated == None:
            parent_categories_deleted_ids = []
            sub_categories_deleted_ids = []
            parent_categories = self.get_parent_categories(session)
            sub_categories = self.get_sub_categories(session)   
        else:
            parent_categories_deleted = get_base_query(session, models.ParentCategory, visible=False).filter(models.ParentCategory.last_updated > last_updated)
            parent_categories_deleted_ids = []
            sub_categories_deleted_ids = []
            for parent_category in parent_categories_deleted:
                parent_categories_deleted_ids.append(parent_category.id)
                for sub_category in parent_category.sub_categories:
                    sub_categories_deleted_ids.append(sub_category.id)
            parent_categories = self.get_parent_categories_after_date_time(session, last_updated)
            sub_categories = self.get_sub_categories_after_date_time(session, last_updated)
            
        result = {"deleted_parent_category_ids": parent_categories_deleted_ids,
         "deleted_sub_category_ids": sub_categories_deleted_ids,
         "parent_categories": parent_categories,
         "sub_categories": sub_categories}
        return result
        
#======================= PARENT CATEGORY CODE ===========================
    def add_parent_category(self, kv_pairs, session):
        validator = validation.ParentCategoryValidator(session)
        validator.validate_kv_pairs(kv_pairs)
        kv_pairs[Keys.User.last_updated] = datetime.datetime.utcnow()
        parent_category = models.ParentCategory(**kv_pairs)
        session.add(parent_category)
    def get_parent_categories(self, session, kv_pairs={}, visible=True):
        parent_categories = get_base_query(session, models.ParentCategory, visible=visible).filter_by(**kv_pairs)
        return rows_to_dict_list(parent_categories)
    def get_parent_categories_after_date_time(self, session, date_time):
        parent_categories = get_base_query(session, models.ParentCategory).filter(models.ParentCategory.last_updated > date_time)
        return rows_to_dict_list(parent_categories)
    def change_parent_category_visibility(self, session, parent_id, visibility):
        parent = session.query(models.ParentCategory).get(parent_id)
        parent.visible = visibility
        parent.last_updated = datetime.datetime.utcnow()
        sub_categories = session.query(models.SubCategory).filter_by(**{Keys.SubCategory.parent_category_id: parent_id})
        for sub_category in sub_categories:
            sub_category.visible = visibility
            sub_category.last_updated = datetime.datetime.utcnow()
            session.add(sub_category)
        session.add(parent)
    def get_parent_category_name_by_id(self, session, parent_category_id):
        parent = session.query(models.ParentCategory).get(parent_category_id)
        return parent.name
    def contains_parent_category_by_name(self, session, name):
        return session.query(models.ParentCategory).filter(func.lower(models.ParentCategory.name) == func.lower(name)).count() > 0
    def contains_parent_category(self, session, kv_pairs):
        return session.query(models.ParentCategory).filter_by(**kv_pairs).count() > 0
#======================= SUB CATEGORY CODE ==============================
    def add_sub_category(self, kv_pairs, session):
        validator = validation.SubCategoryValidator()
        validator.validate_kv_pairs(kv_pairs)
        kv_pairs[Keys.User.last_updated] = datetime.datetime.utcnow()
        sub_category = models.SubCategory(**kv_pairs)
        session.add(sub_category)
    def get_sub_categories(self, session, kv_pairs=[]):
        if kv_pairs == []:
            sub_categories = get_base_query(session, models.SubCategory)
        else:
            sub_categories = session.query(models.SubCategory).filter_by(**kv_pairs)
        return rows_to_dict_list(sub_categories)
    def get_sub_category_name_by_id(self, session, sub_category_id):
        sub_category = session.query(models.SubCategory).get(sub_category_id)
        return sub_category.name
    def is_sub_category_id_visible(self, session, sub_category_id):
        sub_category = session.query(models.SubCategory).get(sub_category_id)
        return sub_category.visible
    def get_parent_category_name_for_sub_category_id(self, session, sub_category_id):
        sub_category = session.query(models.SubCategory).get(sub_category_id)
        return self.get_parent_category_name_by_id(session, sub_category.parent_category_id)
    def get_sub_categories_by_parent_id(self, session, parent_id, kv_pairs={}):
        kv_pairs[Keys.SubCategory.parent_category_id] = parent_id
        sub_categories = get_base_query(session, models.SubCategory).filter_by(**kv_pairs)        
        return rows_to_dict_list(sub_categories)
    def get_sub_categories_after_date_time(self, session, time):
        sub_categories = get_base_query(session, models.SubCategory).filter(models.SubCategory.last_updated > time)        
        return rows_to_dict_list(sub_categories)
    def change_sub_category_visibility(self, session, sub_category_id, visible):
        sub_category = session.query(models.SubCategory).get(sub_category_id)
        parent_category_id = getattr(sub_category, Keys.SubCategory.parent_category_id)
        parent_category = session.query(models.ParentCategory).filter_by(**{Keys.ParentCategory.id: parent_category_id}).first()
        if visible and not parent_category.visible:
            raise custom_exceptions.ConsistencyException("ERROR: Cannot restore sub-category until parent-category has been restored")
            return
        sub_category.visible = (1 if visible else 0)
        sub_category.last_updated = datetime.datetime.utcnow()
        session.add(sub_category)
#============================ USER CODE ================================
    def add_user(self, kv_pairs, session):
        validator = validation.UserValidator()
        validator.validate_kv_pairs(kv_pairs)
        kv_pairs[Keys.User.last_updated] = datetime.datetime.utcnow()
        user = models.User(**kv_pairs)
        session.add(user)
    def get_users(self, session, kv_pairs={}, visible=True):
        users = get_base_query(session, models.User, visible=visible).filter_by(**kv_pairs)
        return rows_to_dict_list(users)
    def get_user_name_from_id(self, session, user_id):
        user = session.query(models.User).get(user_id)
        return user.full_name
    def get_users_after_date_time(self, session, date_time, visible=True):
        users = get_base_query(session, models.User, visible=visible).filter(models.User.last_updated > date_time)
        return rows_to_dict_list(users)
    def get_num_users(self, session):
        return session.query(models.User).count()
    def change_user_visibility(self, session, user_id, visibility):
        user = session.query(models.User).get(user_id)
        user.visible = visibility
        user.last_updated = datetime.datetime.utcnow()
        session.add(user)
#============================== DOG CODE =================================
    def add_dog(self, kv_pairs, session):
        validator = validation.DogValidator()
        validator.validate_kv_pairs(kv_pairs)
        kv_pairs[Keys.User.last_updated] = datetime.datetime.utcnow()
        image_encoded = kv_pairs[Keys.Dog.image]
        image = base64.b64decode(image_encoded)
        image_thumb = image_utils.get_thumbnail(image)

        del kv_pairs[Keys.Dog.image]
                
        dog = models.Dog(**kv_pairs)
        session.add(dog)
        session.flush()
        print dog.id
        
        image_name = "dog_image_" + str(dog.id)
        image_thumb_name = "dog_image_thumb_" + str(dog.id)
        gcs_utils.save_image(image_name, image)
        gcs_utils.save_image(image_thumb_name, image_thumb)
    def update_dog(self, kv_pairs, session):
        save_image = False
        if Keys.Dog.image in kv_pairs:
            image_encoded = kv_pairs[Keys.Dog.image]
            image = base64.b64decode(image_encoded)
            image_thumb = image_utils.get_thumbnail(image)
            del kv_pairs[Keys.Dog.image]
            save_image = True
        dog = session.query(models.Dog).get(int(kv_pairs[Keys.Dog.id]))
        del kv_pairs[Keys.Dog.id]
        kv_pairs[Keys.Dog.last_updated] = datetime.datetime.utcnow()
        dog = set_object_attributes(dog, kv_pairs)
        session.add(dog)
        if save_image:
            image_name = "dog_image_" + str(dog.id)
            image_thumb_name = "dog_image_thumb_" + str(dog.id)
            gcs_utils.save_image(image_name, image)
            gcs_utils.save_image(image_thumb_name, image_thumb)
    def get_dog_name_from_id(self, session, dog_id):
        dog = session.query(models.Dog).get(dog_id)
        return dog.name   
    def get_dogs(self, session, kv_pairs={}, include_image=True, visible=True):
        dogs = get_base_query(session, models.Dog, visible=visible).filter_by(**kv_pairs)
        return self.rows_to_dict_list_dog(dogs, include_image=include_image)
    def change_dog_visibility(self, session, dog_id, visibility):
        dog = session.query(models.Dog).get(dog_id)
        dog.visible = visibility
        dog.last_updated = datetime.datetime.utcnow()
        session.add(dog)
    def get_dogs_after_date_time(self, session, date_time, visible=True):
        dogs = get_base_query(session, models.Dog, visible=visible).filter(models.Dog.last_updated > date_time)
        return self.rows_to_dict_list_dog(dogs)
    def get_num_dogs(self, session):
        return session.query(models.Dog).count()
    def rows_to_dict_list_dog(self, rows, include_image=True):
        my_list = []
        for row in rows:
            kv_pairs = row_to_dict(row)
            if include_image:
                image_name = "dog_image_thumb_" + str(kv_pairs[Keys.Dog.id])
                print "looking for: " + image_name
                image_thumb = gcs_utils.read_file(image_name)
                kv_pairs[Keys.Dog.image] = base64.b64encode(image_thumb)
            kv_pairs[Keys.Dog.birth_date] = kv_pairs[Keys.Dog.birth_date].strftime("%Y-%m-%d")
            my_list.append(kv_pairs)
        return my_list
#============================ ENTRY CODE ==================================
    def add_entry(self, kv_pairs, session):
        dog_id = kv_pairs[Keys.Entry.dog_id]
        if session.query(models.Dog).get(dog_id) == None:
            raise custom_exceptions.DogDoesNotExist("Invalid dog ID: " + str(dog_id))
        user_id = kv_pairs[Keys.Entry.user_id]
        if session.query(models.User).get(user_id) == None:
            raise custom_exceptions.UserDoesNotExist("Invalid user ID: " + str(user_id))
        validator = validation.EntryValidator()
        validator.validate_kv_pairs(kv_pairs)
        kv_pairs[Keys.User.last_updated] = datetime.datetime.utcnow()
        entry = models.Entry(**kv_pairs)
        session.add(entry)
        session.flush()
        return entry.id
    def get_entries(self, session, kv_pairs=[]):
        if kv_pairs == []:
            entries = get_base_query(session, models.Entry)
        else:
            entries = get_base_query(session, models.Entry).filter_by(**kv_pairs)
        return rows_to_dict_list(entries)
    def get_entries_for_display(self, session, visible=True, kv_pairs={}):
        entries = get_base_query(session, models.Entry, visible=visible).filter_by(**kv_pairs)
        entries_list = []
        for row in entries:
            entry = row_to_dict(row)
            entry["dog_name"] = self.get_dog_name_from_id(session, entry[Keys.Entry.dog_id])
            entry["trainer_name"] = self.get_user_name_from_id(session, entry[Keys.Entry.user_id])
            entry["sub_category_name"] = self.get_sub_category_name_by_id(session, entry[Keys.Entry.sub_category_id])
            entry["parent_category_name"] = self.get_parent_category_name_for_sub_category_id(session, entry[Keys.Entry.sub_category_id])
            entries_list.append(entry)
        return entries_list
    def get_entries_after_date_time(self, session, date_time, kv_pairs={}, visible=True):
        entries = get_base_query(session, models.Entry, visible=visible).filter(models.Entry.last_updated > date_time)
        entries = entries.filter_by(**kv_pairs)
        return rows_to_dict_list(entries)
    def clear_plans_for_dogs_subcat(self, session, dog_id, sub_category_id):
        entries = get_base_query(session, models.Entry).filter_by(**{Keys.Entry.dog_id: dog_id, Keys.Entry.sub_category_id: sub_category_id, Keys.Entry.trials_result: "NONE"})
        for entry in entries:
            entry.visible = False
            entry.last_updated = datetime.datetime.utcnow()
            session.add(entry)
    '''
    To undelete an entry, there can't be any planned entries of the same sub_cat_id
    The corresponding sub_cat also cannot be deleted
    '''
    def change_entry_visibility(self, session, entry_id, visibility):
        entry = session.query(models.Entry).get(entry_id)
        if visibility and entry.trials_result == "NONE":
            other_entry_planned = get_base_query(session, models.Entry).filter_by(**{Keys.Entry.trials_result: "NONE", 
                                                                                    Keys.Entry.sub_category_id: entry.sub_category_id, 
                                                                                    Keys.Entry.dog_id: entry.dog_id}).count() > 0
            sub_category_visible = self.is_sub_category_id_visible(session, entry.sub_category_id)
            if other_entry_planned or not sub_category_visible:
                return            
        entry.visible = visibility
        entry.last_updated = datetime.datetime.utcnow()
        session.add(entry)
    def get_num_entries(self, session, kv_pairs=[]):
        if kv_pairs == []:
            count = session.query(models.Entry).count()
        else:
            count = session.query(models.Entry).filter_by(**kv_pairs).count()
        return count
#=============================== HELPER CODE ================================
def rows_to_dict_list(rows):
    my_list = []
    for row in rows:
        my_list.append(row_to_dict(row))
    return my_list
   
def row_to_dict(row):
    d = {}
    for column in row.__table__.columns:
        if column.name in ["last_updated", "visible"]:
            continue
        d[column.name] = getattr(row, column.name)
    return d
def set_object_attributes(obj, kv_pairs):
    for attribute in kv_pairs:
        setattr(obj, attribute, kv_pairs[attribute])
    return obj
def get_base_query(session, modelClass, visible=True):
    return session.query(modelClass).filter_by(visible=visible)

        
        
    