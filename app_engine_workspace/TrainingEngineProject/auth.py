'''
Created on Jun 16, 2014

@author: Matthew
'''
from google.appengine.api import users
import data_manager
from backend_keys import Keys
from functools import wraps
import webapp2
import admin_manager
import service

class Permission:
    CREATE = 1
    READ = 2
    UPDATE = 3
    DELETE = 4
def permission_required(permissions):
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            manager_admin = admin_manager.AdminManager()
            session = service.BaseService.Session()
            hasPermission = manager_admin.current_user_has_permissions(permissions, session)
            if not hasPermission:
                webapp2.abort(403)
            return f(*args, **kwargs)
        return decorated_function
    return decorator

def api_key_required(key):
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            api_key = args[0].request.get(Keys.API.api_key_field)
            if api_key != key:
                webapp2.abort(403)
            return f(*args, **kwargs)
        return decorated_function
    return decorator
            
            
class UserAuth():
    def hasPermission(self, permissions):
        manager = data_manager.DataManager()
        user_auth = users.get_current_user()
        if user_auth == None:
            return False
        user = manager.getUserByName(user_auth.nickname())
        if user == {}:
            return False
        has_perm = True
        if Permission.CREATE in permissions:
            has_perm = has_perm and user[Keys.API.perm_create]
        if Permission.READ in permissions:
            has_perm = has_perm and user[Keys.API.perm_read]
        if Permission.UPDATE in permissions:
            has_perm = has_perm and user[Keys.API.perm_update]
        if Permission.DELETE in permissions:
            has_perm = has_perm and user[Keys.API.perm_delete]
        return has_perm

        
        