'''
Created on Aug 26, 2014

@author: Matthew
'''

from auth import permission_required
from auth import Permission
import webapp2
import data_manager
import jinja2
import os
from backend_keys import Keys
from service import BaseService
import handlers_util
import admin_manager
import service
import models
from StringIO import StringIO
import json
import gcs_utils

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__) + "/templates"),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

class URIProvider:
    def delete_entry(self, entry_id):
        return webapp2.uri_for('change_entry_visibility',
                               entry_id=entry_id,
                               visible=False)
    def restore_entry(self, entry_id):
        return webapp2.uri_for('change_entry_visibility',
                               entry_id=entry_id,
                               visible=True)
    def view_deleted_entries(self):
        return webapp2.uri_for('view_entries',
                               visible=False)
    def view_entries(self):
        return webapp2.uri_for('view_entries', visible=True)
    def get_plan_for_entry(self, entry_id):
        return webapp2.uri_for('get_plan_for_entry', entry_id)
    
uriProvider = URIProvider()

            
class ViewEntries(webapp2.RequestHandler):
    @permission_required([Permission.READ, Permission.CREATE]) 
    def get(self):
        visible_string = self.request.get("visible").strip() 
        visible = visible_string == "" or visible_string == "True"
        dog_id = self.request.get("dog_id")
        dog_id = long(dog_id) if dog_id != "" else -1
        print str(visible)
        manager = data_manager.DataManager()
        manager_admin = admin_manager.AdminManager()
        template = JINJA_ENVIRONMENT.get_template('view_entries.html')
        session = BaseService.Session()
        if dog_id != -1:
            entries = manager.get_entries_for_display(session, visible, kv_pairs={Keys.Entry.dog_id: dog_id})
        else:
            entries = manager.get_entries_for_display(session, visible)
        self.response.out.write(template.render({
                                                 "sign_out_url": manager_admin.get_sign_out_url(),
                                                 "user_name": manager_admin.get_current_user_nickname(),
                                                 "entries": entries,
                                                 'COMMON': Keys.Entry.COMMON,
                                                 'FIELDS': Keys.Entry.FIELDS_DISPLAY,
                                                 'URIProvider': uriProvider,
                                                 'visible': visible,
                                                 'categories': manager.get_parent_categories(session, visible=(1 if visible else 0))}))
class GetPlanForEntry(webapp2.RequestHandler):
    @permission_required([Permission.CREATE, Permission.UPDATE])
    def post(self):
        print "here"
        entry_id = long(self.request.get("entry_id"))
        manager = data_manager.DataManager()
        session = service.BaseService.Session()
        entries = manager.get_entries(session, {Keys.Entry.id: entry_id})
        if len(entries) == 0:
            self.response.write("No entry found for id: " + str(entry_id))
            return
        self.response.write(entries[0][Keys.Entry.plan]);
        
class ChangeEntryVisibility(webapp2.RequestHandler):
    @permission_required([Permission.CREATE, Permission.UPDATE])
    def get(self):
        visible = self.request.get('visible') == 'True'
        entry_id = int(self.request.get('entry_id'))
        manager = data_manager.DataManager()
        session = service.BaseService.Session()
        try:
            manager.change_entry_visibility(session, entry_id, visible)
            session.commit()
        except Exception as e:
            session.rollback()
            self.response.out.write(str(e))
            return
        self.redirect(webapp2.uri_for('view_entries'))
        

        