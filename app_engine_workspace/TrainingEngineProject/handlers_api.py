'''
Created on Jun 26, 2014

@author: Matthew
'''

import webapp2
from sqlalchemy import create_engine
from sqlalchemy import *
import data_manager
from backend_keys import Keys
import handlers_util
import service 
from auth import api_key_required
import admin_manager
import json
import os
from google.appengine.api import app_identity
import cloudstorage as gcs
import datetime
from StringIO import StringIO
#================================ CATEGORIES =============================================
class GetCategoriesCompact(webapp2.RequestHandler):
    @api_key_required(Keys.API.API_KEY)
    def post(self):
        last_updated = self.request.get("last_updated")
        
        manager = data_manager.DataManager()
        kv_pairs = handlers_util.request_to_dict(self.request)
        del kv_pairs[Keys.API.api_key_field]
        session = service.BaseService.Session()
        if last_updated == "NEVER":
            print "NEVER"
            compact = manager.get_category_compact(session)
        else:
            compact = manager.get_category_compact(session, last_updated)
        result = {"data": compact,
         "time": datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")}
        self.response.out.write(json.dumps(result)) 
         
class AddParentCategory(webapp2.RequestHandler):
    @api_key_required(Keys.API.API_KEY)
    def post(self):
        manager = data_manager.DataManager()
        kv_pairs = handlers_util.request_to_dict(self.request)
        del kv_pairs[Keys.API.api_key_field]
        session = service.BaseService.Session()
        try:
            manager.add_parent_category(kv_pairs, session)
            session.commit()
        except Exception as e:
            session.rollback()
            self.response.out.write(str(e))
            return
        self.response.out.write("success")
''' 
STRUCTURE OF PLAN
- array of Question objects
- each Question has a "name" string and "answers" array
- answers is an array of strings
'''
class AddSubCategory(webapp2.RequestHandler):
    @api_key_required(Keys.API.API_KEY)
    def post(self):
        manager = data_manager.DataManager()
        kv_pairs = handlers_util.request_to_dict(self.request)
        del kv_pairs[Keys.API.api_key_field]
        session = service.BaseService.Session()
        try:
            manager.add_sub_category(kv_pairs, session)
            session.commit()
        except Exception as e:
            session.rollback()
            self.response.out.write(str(e))
            return
        self.response.out.write("success")
class AddCredential(webapp2.RequestHandler):
    @api_key_required(Keys.API.API_KEY)
    def post(self):
        manager = admin_manager.AdminManager()
        kv_pairs = handlers_util.request_to_kv_pairs(self, Keys.Credential.FIELDS_ALL, [], Keys.Credential.FIELDS_BOOLEAN)
        print kv_pairs
        session = service.BaseService.Session()
        try:
            manager.add_credential(kv_pairs, session)
            session.commit()
        except Exception as e:
            session.rollback()
            self.response.out.write(str(e))
            return
        self.response.out.write("success")
#================================ USERS =============================================
class AddUser(webapp2.RequestHandler):
    @api_key_required(Keys.API.API_KEY)
    def post(self):
        kv_pairs = handlers_util.request_to_dict(self.request)
        print kv_pairs
        if "last_updated" in kv_pairs:
            self.response.out.write('what is this doing here')
            return
        del kv_pairs["api_key"]
        manager = data_manager.DataManager()
        session = service.BaseService.Session()
        print kv_pairs
        try:
            manager.add_user(kv_pairs, session)
            session.commit()
        except Exception as e:
            session.rollback()
            self.response.out.write(str(e))
            return
        self.response.out.write('success')
        
class GetUsers(webapp2.RequestHandler):
    @api_key_required(Keys.API.API_KEY)
    def post(self):
        last_updated = self.request.get("last_updated")
        manager = data_manager.DataManager()
        session = service.BaseService.Session()
        current_time = datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
        if last_updated == "NEVER":
            users = manager.get_users(session)
            deleted_user_ids = []
        else:
            users = manager.get_users_after_date_time(session, last_updated)  
            deleted_users = manager.get_users_after_date_time(session, last_updated, visible=False)
            deleted_user_ids = []
            for user in deleted_users:
                deleted_user_ids.append(user[Keys.User.id])
        data = {"users": users,
                "deleted_user_ids": deleted_user_ids}
        output = {"data":  data,
                  "time": current_time}       
        self.response.out.write(json.dumps(output))
#================================ DOGS =============================================
class AddDog(webapp2.RequestHandler):
    @api_key_required(Keys.API.API_KEY)
    def post(self):
        kv_pairs = handlers_util.request_to_dict(self.request)
        del kv_pairs["api_key"]
        manager = data_manager.DataManager()
        session = service.BaseService.Session()
        print kv_pairs
        try:
            manager.add_dog(kv_pairs, session)
            session.commit()
        except Exception as e:
            session.rollback()
            print str(e)
            self.response.out.write(str(e))
            return
        self.response.out.write("success")
class UpdateDog(webapp2.RequestHandler):
    @api_key_required(Keys.API.API_KEY)
    def post(self):
        kv_pairs = handlers_util.request_to_dict(self.request)
        del kv_pairs["api_key"]
        manager = data_manager.DataManager()
        session = service.BaseService.Session()
        print kv_pairs
        try:
            manager.update_dog(kv_pairs, session)
            session.commit()
        except Exception as e:
            session.rollback()
            print str(e)
            self.response.out.write(str(e))
            return
        self.response.out.write("success")

class GetDogs(webapp2.RequestHandler):
    @api_key_required(Keys.API.API_KEY)
    def post(self):
        last_updated = self.request.get("last_updated")
        print last_updated
        manager = data_manager.DataManager()
        session = service.BaseService.Session()
        current_time = datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
        if last_updated == "NEVER":
            dogs = manager.get_dogs(session)
            deleted_dog_ids = []
        else:
            dogs = manager.get_dogs_after_date_time(session, last_updated)
            deleted_dogs = manager.get_dogs_after_date_time(session, last_updated, visible=False)
            deleted_dog_ids = []
            for dog in deleted_dogs:
                deleted_dog_ids.append(dog[Keys.Dog.id])
        data = {"dogs": dogs,
                "deleted_dog_ids": deleted_dog_ids}
        output = {"data":  data,
                  "time": current_time}        
        self.response.out.write(json.dumps(output))
#================================ ENTRIES ========================================
class AddEntries(webapp2.RequestHandler):
    @api_key_required(Keys.API.API_KEY)
    def post(self):
        print "in the add entries method"
        kv_pairs = handlers_util.request_to_dict(self.request)
        manager = data_manager.DataManager()
        session = service.BaseService.Session()
        print kv_pairs["entries"]
        entries = json.load(StringIO(kv_pairs["entries"]))
        old_ids = []
        new_ids = []
        for entry in entries:
            old_ids.append(entry[Keys.Entry.id])
            if entry[Keys.Entry.trials_result] == "NONE":
                dog_id = long(entry[Keys.Entry.dog_id])
                sub_category_id = long(entry[Keys.Entry.sub_category_id])
                manager.clear_plans_for_dogs_subcat(session, dog_id, sub_category_id)
            new_id = manager.add_entry(entry, session)
            new_ids.append(new_id)
        try:
            session.commit()
        except Exception as e:
            session.rollback()
            print str(e)
            self.response.out.write(str(e))
            return    
        result = {"old_ids": old_ids,
                  "new_ids": new_ids}    
        self.response.out.write(json.dumps(result))
class GetEntries(webapp2.RequestHandler):
    @api_key_required(Keys.API.API_KEY)
    def post(self):
        print "in the get entries method"
        last_updated = self.request.get("last_updated")
        manager = data_manager.DataManager()
        session = service.BaseService.Session()
        current_time = datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
        if last_updated == "NEVER":
            entries = manager.get_entries(session)
            deleted_entry_ids = []
        else:
            entries = manager.get_entries_after_date_time(session, last_updated)
            deleted_entries = manager.get_entries_after_date_time(session, last_updated, visible=False)
            deleted_entry_ids = []
            for entry in deleted_entries:
                deleted_entry_ids.append(entry[Keys.Entry.id])
        data = {"entries": entries,
                "deleted_entry_ids": deleted_entry_ids}        
        output = {"data":  data,
                  "time": current_time}        
        self.response.out.write(json.dumps(output))
        

