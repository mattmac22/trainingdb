'''
Created on Jun 21, 2014

@author: Matthew
'''
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Boolean, Date, DateTime, ForeignKey
from sqlalchemy import create_engine
from service import BaseService
from sqlalchemy.orm import relationship, backref
import datetime

Base = declarative_base()

class ParentCategory(Base):
    __tablename__ = 'parent_category'
    id = Column(Integer, primary_key=True)
    name = Column(String(60))
    sub_categories = relationship('SubCategory', backref="parent_category")
    last_updated = Column(DateTime)
    visible = Column(Boolean, default=True)

class SubCategory(Base):
    __tablename__ = 'sub_category'
    id = Column(Integer, primary_key=True)
    parent_category_id = Column(Integer, ForeignKey('parent_category.id'), nullable=False)
    name = Column(String(60), nullable=False)
    plan = Column(String(5000), nullable=False)
    visible = Column(Boolean, default=True)
    last_updated = Column(DateTime)
    entries = relationship('Entry', backref="sub_category")
    
    DISPLAY_COMMON = {"id": "ID",
                      "parent_category_id": "Parent ID",
                      "name": "Name",
                      "plan": "Plan",
                      "visible": "Visible",
                      "entries": "Entries"}
    DISPLAY_FIELDS = ["id", "name"]

class User(Base):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True)
    full_name = Column(String(60))
    user_name = Column(String(60))
    password = Column(String(60))
    email = Column(String(100))
    phone = Column(String(60))
    entries = relationship('Entry', backref='user')
    last_updated = Column(DateTime)
    visible = Column(Boolean, default=True)
    def __repr__(self):
        return "<User(user_name='%s', full_name='%s', password='%s', email='%s', phone='%s')>" % (
                             self.user_name, self.full_name, self.password, self.password, self.email, self.phone)
class Dog(Base):
    __tablename__ = 'dog'
    id = Column(Integer, primary_key=True)
    name = Column(String(60))
    entries = relationship('Entry', backref='dog')
    birth_date = Column(Date)
    breed = Column(String(60))
    service_type = Column(String(60))
    visible = Column(Boolean, default=True)
    last_updated = Column(DateTime)
    
class Entry(Base):
    __tablename__ = "entry"
    id = Column(Integer, primary_key=True)
    session_date = Column(DateTime)
    plan = Column(String(500))
    trials_result = Column(String(50))
    last_updated = Column(DateTime)
    dog_id = Column(Integer, ForeignKey('dog.id'))
    user_id = Column(Integer, ForeignKey('user.id'))
    sub_category_id = Column(Integer, ForeignKey('sub_category.id'))
    visible = Column(Boolean, default=True)
    
class Credential(Base):
    __tablename__ = "credentials"
    email = Column(String(100))
    id = Column(Integer, primary_key=True)
    perm_create = Column(Boolean, default=True)
    perm_read = Column(Boolean, default=True)
    perm_update = Column(Boolean, default=True)
    perm_delete =Column(Boolean, default=True)
    
class Log(Base):
    __tablename__ = "logs"
    id = Column(Integer, primary_key=True)
    user_name = Column(String(60))
    date_time = Column(DateTime)
    message = Column(String(500))

def create_tables(connection):
    Base.metadata.create_all(connection) # was BaseService.engine instead of connection
def drop_all_tables(connection):
    Base.metadata.drop_all(connection)

    

