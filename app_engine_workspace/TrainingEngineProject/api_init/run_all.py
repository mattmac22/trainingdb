'''
Created on Jun 23, 2014

@author: Matthew
'''
import credentials
import parent_categories

'''
If running on localhost use "localhost" as the address. Don't forget
to provide the port number as the second argument
'''
location = "localhost"
port = 8888
parent_categories.main(location, port)
credentials.main(location, port)
