'''
Created on Jun 23, 2014

@author: Matthew
'''
import httplib
import urllib
from backend_keys import Keys

def main(address, port=None):
    if port == None:
        conn = httplib.HTTPConnection(address)
    else:
        conn = httplib.HTTPConnection(address, port)
    
    addCategory(conn, "Direction and Control")
    addCategory(conn, "Agility")
    addCategory(conn, "Foundational Obedience")
    addCategory(conn, "Search")

        
def addCategory(conn, category_name):
    headers = {"Content-type": "application/x-www-form-urlencoded",
                    "Accept": "text/plain"}
    params = urllib.urlencode({Keys.API.api_key_field: Keys.API.API_KEY,
                               Keys.ParentCategory.name: category_name})
    conn.request('POST','/api/addParentCategory', params, headers)
    r = conn.getresponse()
    response = r.read()
    if (response == "success"):
        print "Success adding category: " + category_name
    else:
        print "Failure adding category: " + category_name
        
        