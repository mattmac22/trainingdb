'''
Created on Jun 23, 2014

@author: Matthew
'''
import httplib
import urllib
from backend_keys import Keys

def main(address, port=None):
    if port == None:
        conn = httplib.HTTPConnection(address)
    else:
        conn = httplib.HTTPConnection(address, port)
    
    addCredential(conn, "admin@gmail.com")

        
def addCredential(conn, email, perm_create=True, perm_read=True, perm_update=True, perm_delete=True):
    headers = {"Content-type": "application/x-www-form-urlencoded",
                    "Accept": "text/plain"}
    params = urllib.urlencode({Keys.API.api_key_field: Keys.API.API_KEY,
                               Keys.Credential.email: email,
                               Keys.Credential.perm_create: perm_create,
                               Keys.Credential.perm_read: perm_read,
                               Keys.Credential.perm_update: perm_update,
                               Keys.Credential.perm_delete: perm_delete})
    conn.request('POST','/api/addCredential', params, headers)
    r = conn.getresponse()
    response = r.read()
    if (response == "success"):
        print "Success adding credential: " + email
    else:
        print "Failure adding credential: " + email
        
main("localhost", 7777)
        
        