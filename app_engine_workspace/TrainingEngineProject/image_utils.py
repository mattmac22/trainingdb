'''
Created on Aug 17, 2014

@author: Matthew
'''
from google.appengine.api import images

def get_thumbnail(image):
    return images.resize(image, 100, 100)
