'''
Created on Jun 21, 2014

@author: Matthew
'''

class Keys:
    class API:
        API_KEY = "32lk4j2lk3j42lk3"
        api_key_field = "api_key"
        perm_create = "perm_create"
        perm_read = "perm_read"
        perm_update = "perm_update"
        perm_delete = "perm_delete"
        
    class User:
        table_name = 'user'
        id = "id"
        full_name = "full_name"
        user_name = "user_name"
        password = "password"
        email = "email"
        phone = "phone"
        visible = "visible"
        last_updated = "last_updated"
        COMMON = {id: "ID",
                  full_name: "Full Name",
                  user_name: "User Name",
                  password: "Password",
                  email: "Email",
                  phone: "Phone",
                  visible: "Visible",
                  last_updated: "Last Updated"}
        FIELDS_ALL = [full_name, password, email, phone, user_name] 
        FIELDS_DISPLAY = [id, full_name, user_name, email, phone]
        FIELDS_REQUIRED = [full_name, password, email, phone]
    class Entry:
        id = "id"
        table_name = "entry"
        session_date = "session_date"
        plan = "plan"
        trials_result = "trials_result"
        user = "user"
        dog = "dog"
        dog_id = "dog_id"
        user_id = "user_id"
        last_updated = "last_updated"
        visible = "visible"
        sub_category_id = "sub_category_id"
        COMMON= {id: "ID",
                 "dog_name": "Dog",
                 "trainer_name": "Trainer",
                 "sub_category_name": "Category",
                 "parent_category_name": "Parent Category",
                 session_date: "Session Date",
                 trials_result: "Trials Result",
                 }
        FIELDS_ALL = [id, session_date, plan, trials_result, user_id, dog_id, sub_category_id]
        FIELDS_DISPLAY = [id, "dog_name", "trainer_name", "sub_category_name", "parent_category_name", session_date, trials_result]
        FIELDS_REQUIRED = [session_date, plan, trials_result, user_id, dog_id, sub_category_id]
            
    class Dog:
        table_name = "dog"
        id = "id"
        name = "name"
        entries = "entries"
        birth_date = "birth_date"
        breed = "breed"
        service_type = "service_type"
        image = "image"
        visible = "visible"
        last_updated = "last_updated"
        COMMON = {id: "ID",
                  name: "Name",
                  entries: "Entries",
                  birth_date: "Birth Date",
                  breed: "Breed",
                  service_type: "Service Type",
                  image: "Image",
                  visible: "Visible",
                  last_updated: "Last Updated"}
        FIELDS_ALL = [name, entries, birth_date, breed, service_type, image, visible]
        FIELDS_DISPLAY = [id, name, birth_date, breed, service_type]
        FIELDS_REQUIRED = [name, birth_date, breed, service_type]
    class ParentCategory:
        table_name = 'parent_category'
        id = "id"
        name = "name"
        sub_categories = "sub_categories"
        visible = "visible"
        last_updated = "last_updated"
        FIELDS_ALL = [id, name, sub_categories, visible]
        FIELDS_REQUIRED = [name]
        DISPLAY_FIELDS = [id, name]
        DISPLAY_FIELDS_COMMON = ["ID", "Name"]
    class SubCategory:
        table_name = "sub_category"
        id = "id"
        parent_category = "parent_category"
        parent_category_id = "parent_category_id"
        name = "name"
        visible = "visible"
        last_updated = "last_updated"
        plan = "plan"
        COMMON = {id: "ID",
                        name: "Name",
                        plan: "Plan"
                        }
        FIELDS_ALL = [id, parent_category, parent_category_id, name, visible, plan]
        FIELDS_REQUIRED = [name, parent_category_id, plan]
        DISPLAY_FIELDS = [id, name, plan]
    class Question:
        name = "name"
        answers = "answers"
    
    class Credential:
        table_name = "credentials"
        id = "id"
        email = "email"
        perm_create = "perm_create"
        perm_read = "perm_read"
        perm_update = "perm_update"
        perm_delete = "perm_delete"
        FIELDS_ALL = [email, perm_create, perm_read, perm_update, perm_delete]
        FIELDS_BOOLEAN = [perm_create, perm_read, perm_update, perm_delete]
        FIELDS_REQUIRED = [email]
    class Log:
        table_name = "logs"
        user_name = "user_name"
        date_time = "date_time"
        message = "message"
        FIELDS_ALL = [user_name, date_time, message]
        FIELDS_REQUIRED = [user_name, message]


    TABLE_NAMES = [SubCategory.table_name, ParentCategory.table_name, Entry.table_name, User.table_name, Dog.table_name]
