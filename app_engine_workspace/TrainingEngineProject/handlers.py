import webapp2
from sqlalchemy import create_engine
from sqlalchemy import *
import data_manager
from backend_keys import Keys
import jinja2
import os
import admin_manager
import service

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__) + "/templates"),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

class MainHandler(webapp2.RequestHandler):
    def get(self):
        self.response.headers['Content-Type'] = 'text/html'
        template = JINJA_ENVIRONMENT.get_template('main.html')
        manager = admin_manager.AdminManager()
        session = service.BaseService.Session()
        self.response.out.write(template.render({
                                                 "sign_out_url": manager.get_sign_out_url(),
                                                 "user_name": manager.get_current_user_nickname(),
                                                 "permissions": manager.get_current_user_permissions(session)}))